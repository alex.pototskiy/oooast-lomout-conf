@file:Import("../entity/OnecProductAddon.lomout.kts")
@file:Import("../entity/GroupMapping.lomout.kts")
@file:Import("../entity/CategoryTree.lomout.kts")

import net.pototskiy.apps.lomout.api.*
import net.pototskiy.apps.lomout.api.callable.AttributeBuilder
import net.pototskiy.apps.lomout.api.document.Document
import org.bson.types.ObjectId
import org.cache2k.Cache2kBuilder
import org.jetbrains.kotlin.script.util.Import
import org.litote.kmongo.eq
import java.lang.ref.WeakReference
import CategoryTree_lomout.CategoryTree as CT
import GroupMapping_lomout.GroupMapping as GM
import OnecProductAddon_lomout.OnecProductAddon as OPA

class OnecProductCategories : AttributeBuilder<List<String>>() {
    var pathCategorySeparator: String = "/"
    var pathPrefix: String = ""
    /**
     * Onec product categories list builder. It gets site group codes from the product addon
     * or from group mapping and build path string.
     *
     * @param entity The entity to build categories
     * @return R? Categories
     */
    override operator fun invoke(entity: Document, context: LomoutContext): List<String> {
        val repository = context.repository
        val logger = context.logger
        return try {
            val addonType = OPA::class
            val addon = repository.get(addonType, OPA::sku eq entity.getAttribute("sku")!!) as? OPA
            var groups = if (addon?.site_category_codes != null) {
                addon.site_category_codes
            } else {
                val mappingType = GM::class
                val group = repository.get(mappingType, GM::onec_code eq entity.getAttribute("group")!!) as? GM
                        ?: throw AppDataException(
                                suspectedLocation(),
                                "1C group '${entity.getAttribute("group")!!}' has no mapping to magento category code."
                        )
                listOf(group.mage_code)
            }
            groups?.map { buildPath(it, context) } ?: emptyList()
        } catch (e: AppDataException) {
            logger.error("Cannot build 1C product categories, ${(e.suspectedLocation + entity).describeLocation()}.")
            e.causesList { logger.error(it) }
            logger.debug("Exception:", e)
            emptyList()
        } catch (e: AppConfigException) {
            logger.error("Cannot build 1C product categories, ${(e.suspectedLocation + entity).describeLocation()}.")
            e.causesList { logger.error(it) }
            logger.debug("Exception:", e)
            emptyList()
        } catch (e: Exception) {
            logger.error("Cannot build 1C product categories.")
            e.causesList { logger.error(it) }
            logger.debug("Exception:", e)
            emptyList()
        }
    }

    private fun buildPath(code: String, context: LomoutContext): String {
        val repository = context.repository
        val categoryTreeType = CT::class
        val pathList = mutableListOf<String>()
        var current = code
        while (current != "00.00.00.00.00") {
            val category = repository.get(categoryTreeType, listOf(CT::name), CT::skg eq current, includeDeleted = false) as? CT
                    ?: throw AppDataException(suspectedLocation(), "Magento category with code '$current' is not found.")
            pathList.add(category.name)
            current = current.replace(
                    Regex("""(((?!00)[0-9]{2})?(\.(?!00)[0-9]{2})*)((\.?(?!00)[0-9]{2})((\.00)*)$)""")
            ) {
                "${if (it.groupValues[1].isEmpty()) "" else it.groupValues[1] + "."}00${it.groupValues[6]}"
            }
        }
        return "${pathPrefix}${pathList.reversed().joinToString(pathCategorySeparator)}"
    }

    companion object {
        private val categoriesCache = object : Cache2kBuilder<ObjectId, CachedCategories>() {}
                .name("onecProductCategoriesCache")
                .enableJmx(true)
                .entryCapacity(6000)
                .eternal(true)
                .build()
    }

    data class CachedCategories(val value: WeakReference<List<String>>)
}
