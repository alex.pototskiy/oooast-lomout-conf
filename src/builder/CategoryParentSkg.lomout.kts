import CategoryTree_lomout.CategoryTree
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.callable.AttributeBuilder

class CategoryParentSkg : AttributeBuilder<String>() {

    override operator fun invoke(entity: Document, context: LomoutContext): String {
        entity as CategoryTree
        val skg = entity.skg
        return when {
            skg == "OooAst.Catalog" -> "Default.Catalog"
            skg.matches(Regex("""\d{2}\.00\.00\.00\.00""")) -> "OooAst.Catalog"
            else -> {
                val splitSkg = skg.split(".").toMutableList()
                val last = splitSkg.withIndex().findLast { it.value != "00" }
                splitSkg[last?.index ?: 0] = "00"
                splitSkg.joinToString(".")
            }
        }
    }
}
