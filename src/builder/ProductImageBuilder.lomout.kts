@file:Import("../helper/ProductMediaProcessor.lomout.kts")

import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.callable.AttributeBuilder
import org.jetbrains.kotlin.script.util.Import
import ProductMediaProcessor_lomout.ImageBase
import ProductMediaProcessor_lomout.ImageType
import ProductMediaProcessor_lomout.ProductImage
import ProductMediaProcessor_lomout.ProductImageMap
import net.pototskiy.apps.lomout.api.LomoutContext
import java.io.File

class ProductImageBuilder : AttributeBuilder<ProductImageMap>() {
    lateinit var imageBase: ImageBase

    override operator fun invoke(entity: Document, context: LomoutContext): ProductImageMap {
        val images = mutableListOf<ProductImage>()
        @Suppress("UNCHECKED_CAST")
        val hideFromProductPage = entity.getAttribute("hide_from_product_page") as List<String>?
        val sku = entity.getAttribute("sku") as String
        entity.getAttribute("base_image")?.let {
            val file = it as String
            images.add(ProductImage(
                    sku,
                    imageBase,
                    ImageType.BASE,
                    File(removeStartMageSlash(file)),
                    entity.getAttribute("base_image_label") as String? ?: findLabel(entity, file),
                    hideFromProductPage?.contains(file) ?: false
            ))
        }
        entity.getAttribute("small_image")?.let {
            val file = it as String
            images.add(ProductImage(
                    sku,
                    imageBase,
                    ImageType.SMALL,
                    File(removeStartMageSlash(file)),
                    entity.getAttribute("small_image_label") as String? ?: findLabel(entity, file),
                    hideFromProductPage?.contains(file) ?: false
            ))
        }
        entity.getAttribute("thumbnail_image")?.let {
            val file = it as String
            images.add(ProductImage(
                    sku,
                    imageBase,
                    ImageType.THUMBNAIL,
                    File(removeStartMageSlash(file)),
                    entity.getAttribute("thumbnail_image_label") as String? ?: findLabel(entity, file),
                    hideFromProductPage?.contains(file) ?: false
            ))
        }
        entity.getAttribute("swatch_image")?.let {
            val file = it as String
            images.add(ProductImage(
                    sku,
                    imageBase,
                    ImageType.SWATCH,
                    File(removeStartMageSlash(file)),
                    entity.getAttribute("swatch_image_label") as String? ?: findLabel(entity, file),
                    hideFromProductPage?.contains(file) ?: false
            ))
        }
        @Suppress("UNCHECKED_CAST")
        val addImageLabels = (entity.getAttribute("additional_image_labels") as List<String>?) ?: emptyList()
        @Suppress("UNCHECKED_CAST")
        (entity.getAttribute("additional_images") as List<String>?)?.forEachIndexed { index, stringFile: String ->
            val file = File(removeStartMageSlash(stringFile))
            if (images.find { it.file == file } == null) {
                images.add(ProductImage(
                        sku,
                        imageBase,
                        ImageType.ADDITIONAL,
                        file,
                        addImageLabels.getOrNull(index),
                        hideFromProductPage?.contains(stringFile) ?: false
                ))
            }
        }
        return ProductImageMap(images.associateBy { it.normalizedFile })
    }

    @Suppress("UNCHECKED_CAST")
    private fun findLabel(entity: Document, file: String): String? {
        return (entity.getAttribute("additional_images") as List<String>?)?.indexOf(file)?.let {
            (entity.getAttribute("additional_image_labels") as List<String>?)?.getOrNull(it)
        }
    }

    private fun removeStartMageSlash(file: String): String =
            if (imageBase == ImageBase.MAGE && file[0] == '/') file.drop(1) else file
}
