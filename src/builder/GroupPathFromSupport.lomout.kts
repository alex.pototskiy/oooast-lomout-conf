class GroupPathFromSupport : AttributeBuilderPlugin<StringType>() {
    override fun build(entity: DbEntity): StringType? {
        val type = entity.entityType
        val groupCode = entity["code"] ?: return null
        val support = DbEntity.getByAttribute(type, type["code"], groupCode).firstOrNull()
        return support?.get("magento_path") as? StringType?
    }
}
