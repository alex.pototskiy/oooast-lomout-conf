@file:Import("../helper/ServerData.lomout.kts")
@file:Import("../entity/OnecProductAddon.lomout.kts")

import ServerData_lomout.ServerMachineVendor
import ServerData_lomout.ServerPartVendor
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.document.Document
import org.jetbrains.kotlin.script.util.Import
import org.litote.kmongo.eq
import OnecProductAddon_lomout.OnecProductAddon as OPA

object ExtractOnecVendor {
    fun extractMachineVendor(entity: Document): List<String> {
        val context = LomoutContext.getContext()
        val sku = entity.getAttribute("sku")
        val machineVendors = vendors(entity, ServerMachineVendor.labelToValue.keys)
        val partVendors = vendors(entity, ServerPartVendor.labelToValue.keys)
        return if (machineVendors.isNotEmpty()) {
            machineVendors
        } else {
            val v = entity.getAttribute("machine_vendor") as? String
            if (v != null && v.isNotBlank() && partVendors.isEmpty()) {
                context.logger.warn("Onec product with sku '$sku' has not empty machine vendor '$v' but nothing can be recognized.")
            }
            listOf("Not Defined")
        }
    }

    fun extractPartVendor(entity: Document): List<String> {
        val context = LomoutContext.getContext()
        val sku = entity.getAttribute("sku")
        val machineVendors = vendors(entity, ServerMachineVendor.labelToValue.keys)
        val partVendors = vendors(entity, ServerPartVendor.labelToValue.keys)
        return if (partVendors.isNotEmpty()) {
            partVendors
        } else {
            val v = entity.getAttribute("machine_vendor") as? String
            if (v != null && v.isNotBlank() && machineVendors.isEmpty()) {
                context.logger.warn("Onec product with sku '$sku' has not empty machine vendor '$v' but nothing can be recognized.")
            }
            listOf("Not Defined")
        }
    }

    private fun vendors(entity: Document, allowedVendors: Iterable<String>): List<String> {
        val machineVendor = entity.getAttribute("machine_vendor") as? String
        return if (machineVendor != null) {
            val corrected = correctVendor(machineVendor)
            val list = allowedVendors.mapNotNull {
                if (corrected.contains(it.toLowerCase())) {
                    it
                } else {
                    null
                }
            }
            list
        } else {
            emptyList()
        }
    }

    private fun correctVendor(vendors: String): String {
        return correctWholeString(vendors)
                ?: correctSubstring(vendors)
    }

    private fun correctWholeString(vendors: String): String? = wholeStringCorrection[vendors]?.toLowerCase()

    private fun correctSubstring(vendors: String): String {
        var corrected = vendors
        substringCorrection.keys.forEach {
            if (corrected.contains(it)) {
                corrected = corrected.replace(it, substringCorrection.getValue(it))
            }
        }
        return corrected.toLowerCase()
    }

    private val wholeStringCorrection: Map<String, String> = mapOf(
            "CAT" to "Caterpillar",
            "Cat" to "Caterpillar"
    )
    private val substringCorrection: Map<String, String> = mapOf(
            "Caterpilar" to "Caterpillar",
            "Catterpilar" to "Caterpillar",
            "Liebher" to "Liebherr",
            "KATO" to "KATO Works",
            "Kato" to "KATO Works",
            "Коматсу" to "Komatsu",
            "Хитачи" to "Hitachi"
    )
}

