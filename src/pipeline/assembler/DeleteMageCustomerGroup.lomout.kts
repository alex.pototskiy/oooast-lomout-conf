@file:Import("../../entity/ImportCustomerGroup.lomout.kts")
@file:Import("../../entity/MageCustomerGroup.lomout.kts")

import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.PipelineAssembler
import net.pototskiy.apps.lomout.api.causesList
import net.pototskiy.apps.lomout.api.entity.EntityCollection
import org.jetbrains.kotlin.script.util.Import
import ImportCustomerGroup_lomout.ImportCustomerGroup as IG
import MageCustomerGroup_lomout.MageCustomerGroup as MG

class DeleteMageCustomerGroup : PipelineAssembler<IG>() {
    /**
     * Assembler function
     *
     * @param entities The pipeline entity collection
     * @return The transient document
     */
    override fun invoke(entities: EntityCollection, context: LomoutContext): ImportCustomerGroup_lomout.ImportCustomerGroup? {
        return try {
            val group = IG()
            val mageGroup = entities[MG::class] as MG
            group.action = "delete"
            group.groupId = mageGroup.groupId
            group.groupName = mageGroup.groupName
            group.taxClass = mageGroup.taxClass
            group
        } catch (e: Exception) {
            context.logger.error("Cannot create Magento customer group for deletion.", e)
            e.causesList { context.logger.error(it) }
            null
        }
    }

}
