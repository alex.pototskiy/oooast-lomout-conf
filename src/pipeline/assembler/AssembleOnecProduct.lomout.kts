@file:Import("../../entity/OnecProduct.lomout.kts")
@file:Import("../../entity/OnecProductAddon.lomout.kts")
@file:Import("../../entity/MageProduct.lomout.kts")
@file:Import("../../helper/ProductMediaProcessor.lomout.kts")
@file:Import("../../helper/ServerData.lomout.kts")

import ServerData_lomout.ServerCountries
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.PipelineAssembler
import net.pototskiy.apps.lomout.api.entity.EntityCollection
import org.jetbrains.kotlin.script.util.Import
import org.litote.kmongo.eq
import AssembledOnecProduct_lomout.AssembledOnecProduct as AP
import OnecProductAddon_lomout.OnecProductAddon as OPA
import OnecProduct_lomout.OnecProduct as OP

class AssembleOnecProduct : PipelineAssembler<AP>() {
    override operator fun invoke(entities: EntityCollection, context: LomoutContext): AssembledOnecProduct_lomout.AssembledOnecProduct? {
        val repository = context.repository
        val data = AP()
        val onec = entities[OP::class] as OP
        val onecAddon = repository.get(OPA::class, OPA::sku eq onec.sku) as? OPA
        data.sku = onec.sku
        data.product_websites = listOf("base")
        data.categories = onec.categories
        data.attribute_set_code = onecAddon?.attribute_set_code ?: "Part"
        data.product_type = onecAddon?.product_type ?: "simple"
        data.name = onecAddon?.name ?: onec.name!!
        onecAddon?.description?.let { data.description = it }
        onecAddon?.short_description?.let { data.short_description = it }
        onec.weight?.let { data.weight = it }
        setCountryOfManufacture(onec, data, context)
        data.price = onec.price_base ?: 0.1
        data.cost = onec.cost
        data.url_key = "product-${onec.sku}"
        // ooo ast
        data.english_name = onecAddon?.english_name ?: onec.english_name ?: "Not Defined"
        data.machine = onecAddon?.machine ?: "Not Defined"
        data.machine_unit = onecAddon?.machine_unit ?: "Not Defined"
        data.machine_vendor = (onecAddon?.machine_vendor
                ?: onec.machine_vendor_list).run { if (isEmpty()) null else this }
        data.machine_model = (onecAddon?.machine_model ?: onec.machine_model)?.run { if (isEmpty()) null else this }
        data.part_vendor = (onecAddon?.part_vendor ?: onec.part_vendor_list).run { if (isEmpty()) null else this }
        data.catalog_part_number = (onecAddon?.catalog_part_number
                ?: catalogPartNumber(onec))?.run { if (isEmpty()) null else this }
        // dimensions
        onecAddon?.height?.let { data.ts_dimensions_height = it }
        onecAddon?.length?.let { data.ts_dimensions_length = it }
        onecAddon?.width?.let { data.ts_dimensions_width = it }
        // meta info
        onecAddon?.meta_title?.let { data.meta_title = it }
        onecAddon?.meta_description?.let { data.meta_description = it }
        onecAddon?.meta_keywords?.let { data.meta_keywords = it }

        return data
    }

    private fun setCountryOfManufacture(onec: OnecProduct_lomout.OnecProduct, data: AP, context: LomoutContext) {
        val country = onec.country_of_manufacture?.let { ServerCountries.getCountryValue(it) }
        if (country == null && onec.country_of_manufacture != null) {
            context.logger.warn("Country '${onec.country_of_manufacture}' of product with sku '${onec.sku}' is not recognized.")
        }
        country?.let { data.country_of_manufacture = it }
    }

    private fun catalogPartNumber(onec: OP): List<String>? {
        val v = onec.catalog_part_number ?: emptyList()
        return if (onec.catalog_part_number_ext != null) {
            v.plus(onec.catalog_part_number_ext!!)
        } else {
            v
        }
    }
}
