@file:Import("../../entity/ImportProduct.lomout.kts")
@file:Import("../../entity/MageProduct.lomout.kts")

import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.PipelineAssembler
import net.pototskiy.apps.lomout.api.entity.EntityCollection
import org.jetbrains.kotlin.script.util.Import
import ImportProduct_lomout.ImportProduct as IP
import MageProduct_lomout.MageProduct as MP

class DeleteMageProduct : PipelineAssembler<IP>() {
    override operator fun invoke(entities: EntityCollection, context: LomoutContext): ImportProduct_lomout.ImportProduct? {
        val data = IP()
        return try {
            val mage = entities[MP::class] as MP
            data._action = "delete"
            data.sku = mage.sku
            data
        } catch (e: Exception) {
            context.logger.error("Cannot create product delete entity", e)
            null
        }
    }
}
