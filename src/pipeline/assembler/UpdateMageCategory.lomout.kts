import net.pototskiy.apps.lomout.api.document.toAttribute
import net.pototskiy.apps.lomout.api.entity.EntityCollection
import net.pototskiy.apps.lomout.api.plugable.PipelineAssemblerPlugin
import CategoryTree_lomout.CategoryTree as CT
import ImportCategory_lomout.ImportCategory as IC
import MageCategory_lomout.MageCategory as MC

class UpdateMageCategory : PipelineAssemblerPlugin<IC>() {
    override fun assemble(entities: EntityCollection): IC? {
        val data = IC()
        try {
            val mageCategory = entities[MC::class] as MC
            val onecCategory = entities[CT::class] as CT
            data.action = "update"
            mageCategory.store?.let { data.store = it }
            mageCategory.website?.let { data.website = it }
            data.skg = mageCategory.skg
            data.flagIsActive = mageCategory.flagIsActive
            data.displayMode = "Products only"
            data.flagIsAnchor = mageCategory.flagIsAnchor
            data.includeInMenu = mageCategory.includeInMenu
            data.useNameInProductSearch = mageCategory.useNameInProductSearch
            data.parentGroupCode = onecCategory.parentSkg
            onecCategory.image?.let { data.image = it }
            for (attr in mapAttr.keys) {
                val mageAttr = mapAttr.getValue(attr)
                onecCategory.getAttribute(attr)?.let { data.setAttribute(mageAttr, it) }
            }
            return data
        } catch (e: Exception) {
            logger.error("Cannot create category update entity", e)
            return null
        }
    }

    companion object {
        val mapAttr = mapOf(
                CT::name.toAttribute() to IC::name.toAttribute(),
                CT::urlKey.toAttribute() to IC::urlKey.toAttribute(),
                CT::description.toAttribute() to IC::description.toAttribute(),
                CT::image.toAttribute() to IC::image.toAttribute(),
                CT::metaTitle.toAttribute() to IC::metaTitle.toAttribute(),
                CT::metaKeywords.toAttribute() to IC::metaKeywords.toAttribute(),
                CT::metaDescription.toAttribute() to IC::metaDescription.toAttribute()
        )
    }
}
