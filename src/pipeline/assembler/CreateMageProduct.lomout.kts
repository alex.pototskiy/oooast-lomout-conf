@file:Import("../../entity/ImportProduct.lomout.kts")
@file:Import("../../entity/MageProduct.lomout.kts")
@file:Import("../../entity/AssembledOnecProduct.lomout.kts")
@file:Import("../../helper/ProductMediaProcessor.lomout.kts")

import ProductMediaProcessor_lomout.ProductImageMap
import net.pototskiy.apps.lomout.api.AppDataException
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.document.toAttribute
import net.pototskiy.apps.lomout.api.entity.EntityCollection
import net.pototskiy.apps.lomout.api.callable.PipelineAssembler
import net.pototskiy.apps.lomout.api.suspectedLocation
import org.jetbrains.kotlin.script.util.Import
import AssembledOnecProduct_lomout.AssembledOnecProduct as OP
import ImportProduct_lomout.ImportProduct as IP
import ProductMediaProcessor_lomout.ProductMediaProcessor as imageTools

open class CreateMageProduct : PipelineAssembler<IP>() {
    override operator fun invoke(entities: EntityCollection, context: LomoutContext): ImportProduct_lomout.ImportProduct? {
        val mage = IP()
        val onec = entities[OP::class] as OP
        mage._action = "create"
        mage.sku = onec.sku
        mage.product_websites = listOf("base")
        mage.categories = onec.categories!!
        mage.attribute_set_code = onec.attribute_set_code
        mage.product_type = onec.product_type
        mage.name = onec.name
                ?: throw AppDataException(suspectedLocation(OP::class), "Product with sku '${onec.sku}' has no name.")
        mage.url_key = "product-${onec.sku}"
        mage.price = onec.price ?: 0.1
        mage.cost = onec.cost
        // images
        imageAttributes.forEach { (image, attrs) ->
            val (imageAttr, labelAttr) = attrs
            image(onec.images)?.let {
                mage.setAttribute(
                        imageAttr,
                        imageTools.copyMediaFile(it.file, it.sku).path.replace("\\", "/")
                )
                mage.setAttribute(labelAttr, it.label)
            }
        }
        mage.additional_images = onec.images.values.map {
            imageTools.copyMediaFile(it.file, it.sku).path.replace("\\", "/")
        }
        mage.hide_from_product_page = onec.images.values.filter { it.hidden }
                .map { imageTools.getOutputMediaName(it.file, it.sku).path.replace("\\", "/") }
        // attributes
        attributesToCopy.forEach { (mageAttr, onecAttr) ->
            onec.getAttribute(onecAttr)?.let { mage.setAttribute(mageAttr, it) }
        }

        return mage
    }

    companion object {
        val f = ProductImageMap::baseImage
        val imageAttributes = mapOf(
                ProductImageMap::baseImage to listOf(IP::base_image, IP::base_image_label),
                ProductImageMap::smallImage to listOf(IP::small_image, IP::small_image_label),
                ProductImageMap::thumbnailImage to listOf(IP::thumbnail_image, IP::thumbnail_image_label),
                ProductImageMap::swatchImage to listOf(IP::swatch_image, IP::swatch_image_label)
        ).map { entry -> entry.key to entry.value.map { it.toAttribute() } }.toMap()
        val imageLabelAttributes = listOf(
                IP::base_image_label,
                IP::small_image_label,
                IP::thumbnail_image_label,
                IP::swatch_image_label,
                IP::additional_image_labels
        ).map { it.toAttribute() }
        val attributesToCopy = mapOf(
                // Magento attributes
                IP::description to OP::description,
                IP::short_description to OP::short_description,
                IP::country_of_manufacture to OP::country_of_manufacture,
                IP::weight to OP::weight,
                IP::meta_title to OP::meta_title,
                IP::meta_description to OP::meta_description,
                IP::meta_keywords to OP::meta_keywords,
                // OOO Ast attributes
                IP::english_name to OP::english_name,
                IP::machine to OP::machine,
                IP::machine_unit to OP::machine_unit,
                IP::machine_model to OP::machine_model,
                IP::machine_vendor to OP::machine_vendor,
                IP::part_vendor to OP::part_vendor,
                IP::catalog_part_number to OP::catalog_part_number,
                // Former additional attributes
                IP::ratings_summary to OP::ratings_summary,
                IP::ts_dimensions_height to OP::ts_dimensions_height,
                IP::ts_dimensions_length to OP::ts_dimensions_length,
                IP::ts_dimensions_width to OP::ts_dimensions_width
                // Standard attributes
        ).map { it.key.toAttribute() to it.value.toAttribute() }.toMap()
    }
}
