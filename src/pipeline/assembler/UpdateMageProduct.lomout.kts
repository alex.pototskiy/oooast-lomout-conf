@file:Import("../../entity/AssembledOnecProduct.lomout.kts")
@file:Import("../../entity/MageProduct.lomout.kts")
@file:Import("../../entity/ImportProduct.lomout.kts")
@file:Import("../../entity/ImportProductUpdates.lomout.kts")
@file:Import("CreateMageProduct.lomout.kts")

import ImportProductUpdates_lomout.ImportProductUpdates
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.entity.EntityCollection
import net.pototskiy.apps.lomout.api.callable.PipelineAssembler
import org.jetbrains.kotlin.script.util.Import
import ImportProduct_lomout.ImportProduct as IP

class UpdateMageProduct : PipelineAssembler<IP>() {
    override operator fun invoke(entities: EntityCollection, context: LomoutContext): ImportProduct_lomout.ImportProduct? {
        return try {
            val updates = entities[ImportProductUpdates::class] as ImportProductUpdates
            val target = IP()
            updates.documentMetadata.attributes.values.forEach {
                target.setAttribute(it.name, updates.getAttribute(it))
            }
            target._action = "update"
            return target
        } catch (e: Exception) {
            context.logger.error("Cannot create product update entity", e)
            null
        }
    }
}
