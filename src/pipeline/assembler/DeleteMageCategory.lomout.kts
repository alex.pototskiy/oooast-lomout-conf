@file:Import("../../entity/MageCategory.lomout.kts")
@file:Import("../../entity/ImportCategory.lomout.kts")

import net.pototskiy.apps.lomout.api.entity.EntityCollection
import net.pototskiy.apps.lomout.api.plugable.PipelineAssemblerPlugin
import org.jetbrains.kotlin.script.util.Import
import kotlin.reflect.KClass
import ImportCategory_lomout.ImportCategory as IC
import MageCategory_lomout.MageCategory as MC

class DeleteMageCategory : PipelineAssemblerPlugin<IC>() {
    override fun assemble(entities: EntityCollection): IC? {
        val data = IC()
        return try {
            val skg = (entities[MC::class] as MC).skg
            if (skg == "Default.Catalog" || skg == "OooAst.Catalog") {
                data
            } else {
                data.action = "delete"
                data.store = "admin"
                data.skg = skg
                data
            }
        } catch (e: Exception) {
            logger.error("Can create mage category entity to delete", e)
            null
        }
    }

}
