@file:Import("../../entity/ImportCustomerGroup.lomout.kts")
@file:Import("../../entity/OnecCustomerGroup.lomout.kts")
@file:Import("../../entity/MageCustomerGroup.lomout.kts")
@file:Import("../../helper/ServerData.lomout.kts")

import ServerData_lomout.ServerTaxClasses
import net.pototskiy.apps.lomout.api.*
import net.pototskiy.apps.lomout.api.callable.PipelineAssembler
import net.pototskiy.apps.lomout.api.entity.EntityCollection
import org.jetbrains.kotlin.script.util.Import
import ImportCustomerGroup_lomout.ImportCustomerGroup as IG
import MageCustomerGroup_lomout.MageCustomerGroup as MG
import OnecCustomerGroup_lomout.OnecCustomerGroup as OG

class CreateMageCustomerGroup : PipelineAssembler<IG>() {
    /**
     * Assembler function
     *
     * @param entities The pipeline entity collection
     * @return The transient document
     */
    override fun invoke(entities: EntityCollection, context: LomoutContext): ImportCustomerGroup_lomout.ImportCustomerGroup? {
            val group = IG()
            val onecGroup = entities[OG::class] as OG
            val mageGroup = entities.getOrNull(MG::class) as? MG
            if (!ServerTaxClasses.doesTaxClassExist(onecGroup.taxClass)) {
                throw AppDataException(suspectedValue(onecGroup.taxClass) + OG::class, "Tax class does not exist on the server side.")
            }
            group.action = if (mageGroup == null) {
                "add"
            } else {
                "update"
            }
            group.groupId = mageGroup?.groupId ?: -1
            group.groupName = onecGroup.groupName
            group.taxClass = onecGroup.taxClass
            return group
    }

}
