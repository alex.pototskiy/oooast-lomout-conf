@file:Import("../../entity/CategoryTree.lomout.kts")
@file:Import("../../entity/ImportCategory.lomout.kts")

import net.pototskiy.apps.lomout.api.document.toAttribute
import net.pototskiy.apps.lomout.api.entity.EntityCollection
import net.pototskiy.apps.lomout.api.plugable.PipelineAssemblerPlugin
import org.jetbrains.kotlin.script.util.Import
import kotlin.reflect.KClass
import CategoryTree_lomout.CategoryTree as CT
import ImportCategory_lomout.ImportCategory as IC

class CreateMageCategory : PipelineAssemblerPlugin<IC>() {
    override fun assemble(entities: EntityCollection): IC? {
        try {
            val onecCategory = entities[CT::class] as CT
            val data = IC()
            data.action = "add"
            data.action = "admin"
            data.parentGroupCode = onecCategory.parentSkg
            data.skg = onecCategory.skg
            data.flagIsActive = true
            data.displayMode = "Products only"
            data.flagIsAnchor = true
            data.includeInMenu = true
            data.useNameInProductSearch = true
            onecCategory.image?.let { data.image = it }
            for (attr in mapAttr.keys) {
                val mageAttr = mapAttr.getValue(attr)
                onecCategory.getAttribute(attr)?.let { data.setAttribute(mageAttr,it) }
            }
            return data
        } catch (e: Exception) {
            logger.error("Cannot create mage category entity", e)
            return null
        }
    }

    companion object {
        val mapAttr = mapOf(
                CT::name.toAttribute() to IC::name.toAttribute(),
                CT::urlKey.toAttribute() to IC::urlKey.toAttribute(),
                CT::description.toAttribute() to IC::description.toAttribute(),
                CT::image.toAttribute() to IC::image.toAttribute(),
                CT::metaTitle.toAttribute() to IC::metaTitle.toAttribute(),
                CT::metaKeywords.toAttribute() to IC::metaKeywords.toAttribute(),
                CT::metaDescription.toAttribute() to IC::metaDescription.toAttribute()
        )
    }
}
