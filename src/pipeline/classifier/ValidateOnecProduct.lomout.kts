@file:Import("../../entity/OnecProduct.lomout.kts")
@file:Import("../../entity/OnecProductAddon.lomout.kts")

import net.pototskiy.apps.lomout.api.AppDataException
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.PipelineClassifier
import net.pototskiy.apps.lomout.api.script.pipeline.ClassifierElement
import net.pototskiy.apps.lomout.api.suspectedLocation
import org.jetbrains.kotlin.script.util.Import
import org.litote.kmongo.eq
import OnecProduct_lomout.OnecProduct as OP
import OnecProductAddon_lomout.OnecProductAddon as OPA

class ValidateOnecProduct: PipelineClassifier() {
    override operator fun invoke(element: ClassifierElement, context: LomoutContext): ClassifierElement {
        val onec = element.entities[OP::class] as OP
        val onecAddon = context.repository.get(OPA::class, OPA::sku eq onec.sku) as? OPA
        return try {
            checkProductName(onec, onecAddon)
            element.match()
        } catch (e: AppDataException) {
            context.logger.error(e.message)
            element.mismatch()
        }
    }

    private fun checkProductName(onec: OnecProduct_lomout.OnecProduct, onecAddon: OnecProductAddon_lomout.OnecProductAddon?) {
        if (onecAddon?.name == null && onec.name == null) {
            throw AppDataException(suspectedLocation(), "Onec product with sku '${onec.sku}' has no name.")
        }
    }
}
