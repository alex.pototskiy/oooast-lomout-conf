@file:Import("../../entity/MageCustomerGroup.lomout.kts")
@file:Import("../../entity/OnecCustomerGroup.lomout.kts")

import net.pototskiy.apps.lomout.api.AppDataException
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.PipelineClassifier
import net.pototskiy.apps.lomout.api.causesList
import net.pototskiy.apps.lomout.api.script.pipeline.ClassifierElement
import net.pototskiy.apps.lomout.api.suspectedLocation
import org.jetbrains.kotlin.script.util.Import
import org.litote.kmongo.eq
import MageCustomerGroup_lomout.MageCustomerGroup as MG
import OnecCustomerGroup_lomout.OnecCustomerGroup as OG

class TheSameGroupName : PipelineClassifier() {
    /**
     * Classifier function
     *
     * @param element ClassifierElement The element to classify
     * @return ClassifierElement
     */
    override fun invoke(element: ClassifierElement, context: LomoutContext): ClassifierElement {
        val repository = context.repository
        return try {
            val onecGroup = element.entities.getOrNull(OG::class) as? OG
            if (onecGroup != null) {
                val mageGroup = repository.get(MG::class, MG::groupName eq onecGroup.groupName)
                if (mageGroup != null) {
                    element.match(mageGroup)
                } else {
                    element.mismatch()
                }
            } else {
                val mageGroup = element.entities.getOrNull(MG::class) as? MG
                        ?: throw AppDataException(suspectedLocation(), "There is no proper entity in the pipeline.")
                val entity = repository.get(OG::class, OG::groupName eq mageGroup.groupName)
                if (entity != null) {
                    element.skip()
                } else {
                    element.mismatch()
                }
            }
        } catch (e: Exception) {
            context.logger.error("Cannot compare customer group name.", e)
            e.causesList { context.logger.error(it) }
            element.mismatch()
        }
    }

}
