@file:Import("../../entity/MageProduct.lomout.kts")
@file:Import("../../entity/ImportProductUpdates.lomout.kts")
@file:Import("../../helper/ProductMediaProcessor.lomout.kts")
@file:Import("../../helper/Utils.lomout.kts")
@file:Import("../../builder/ProductImageBuilder.lomout.kts")

import ImportProductUpdates_lomout.ImportProductUpdates
import ProductMediaProcessor_lomout.*
import Utils_lomout.ListUtils
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.PipelineClassifier
import net.pototskiy.apps.lomout.api.document.toAttribute
import net.pototskiy.apps.lomout.api.script.pipeline.ClassifierElement
import org.jetbrains.kotlin.script.util.Import
import java.io.File
import AssembledOnecProduct_lomout.AssembledOnecProduct as OP
import MageProduct_lomout.MageProduct as MP
import ProductMediaProcessor_lomout.ProductMediaProcessor as imageTools

class NeedToUpdateProduct : PipelineClassifier() {
    override operator fun invoke(element: ClassifierElement, context: LomoutContext): ClassifierElement {
        var doUpdate = false
        val onec = element.entities[OP::class] as OP
        val mage = element.entities[MP::class] as MP
        val updates = ImportProductUpdates()
        mage.documentMetadata.attributes.values.forEach {
            updates.setAttribute(it.name, mage.getAttribute(it))
        }
        for ((onecAttr, mageAttr) in attrToSimpleCompare) {
            val onecVal = onec.getAttribute(onecAttr)
            val mageVal = mage.getAttribute(mageAttr)
            doUpdate = doUpdate or if (onecAttr.klass == List::class) {
                @Suppress("UNCHECKED_CAST")
                val different = !ListUtils.isListEqual(onecVal as List<Any>?, mageVal as List<Any>?)
                if (different) updates.setAttribute(mageAttr.name, onecVal)
                different
            } else {
                val different = onecVal != mageVal
                if (different) updates.setAttribute(mageAttr.name, onecVal)
                different
            }
        }
        doUpdate = doUpdate or compareProductImages(onec, mage, updates)
        return if (doUpdate) element.match(updates) else element.mismatch()
    }

    private fun compareProductImages(
            onec: AssembledOnecProduct_lomout.AssembledOnecProduct,
            mage: MageProduct_lomout.MageProduct,
            updates: ImportProductUpdates
    ): Boolean {
        val sku = onec.sku
        val imagesToAdd = onec.images.keys.minus(mage.images.keys)
        val imagesLeaveAsIs = mage.images.keys.intersect(onec.images.keys).filter {
            val mageImage = mage.images.getValue(it)
            val onecImage = onec.images.getValue(it)
            mageImage.label == onecImage.label &&
                    mageImage.hidden == onecImage.hidden &&
                    mageImage.type == onecImage.type
        }
        val imagesToLeaveAndUpdate = mage.images.keys.intersect(onec.images.keys).filter {
            val mageImage = mage.images.getValue(it)
            val onecImage = onec.images.getValue(it)
            mageImage.label != onecImage.label ||
                    mageImage.hidden != onecImage.hidden ||
                    mageImage.type != onecImage.type
        }
        val imagesToRemove = mage.images.keys.minus(onec.images.keys)
        val doUpdate = imagesToAdd.isNotEmpty() || imagesToRemove.isNotEmpty() || imagesToLeaveAndUpdate.isNotEmpty()
        val addImages = mutableMapOf<String, ProductImage>()
        if (doUpdate) {
            imagesToRemove.forEach {
                val image = mage.images.getValue(it)
                setTaggedImage(sku, image.type, updates, null, null)
            }
            imagesToAdd.forEach {
                val image = onec.images.getValue(it)
                val file = imageTools.copyMediaFile(image.file, sku)
                setTaggedImage(sku, image.type, updates, file, image.label)
                addImages[imageTools.fileNameInMagento(file, sku)!!.path] = ProductImage(
                        sku,
                        ImageBase.MAGE,
                        image.type,
                        file,
                        image.label,
                        image.hidden
                )
            }
            imagesLeaveAsIs.forEach {
                val image = mage.images.getValue(it)
                addImages[image.file.path] = ProductImage(
                        sku,
                        ImageBase.MAGE,
                        image.type,
                        image.file,
                        image.label,
                        image.hidden
                )
            }
            imagesToLeaveAndUpdate.forEach {
                val mageImage = mage.images.getValue(it)
                val onecImage = onec.images.getValue(it)
                setTaggedImage(sku, mageImage.type, updates, mageImage.file, onecImage.label)
                addImages[mageImage.file.path] = ProductImage(
                        sku,
                        ImageBase.MAGE,
                        onecImage.type,
                        mageImage.file,
                        onecImage.label,
                        onecImage.hidden
                )
            }
            createUpdateForAddImages(addImages, updates)
        }
        return doUpdate
    }

    private fun createUpdateForAddImages(addImages: Map<String, ProductImage>, updates: ImportProductUpdates) {
        if (addImages.isEmpty()) {
            updates.additional_images = null
            updates.additional_image_labels = null
            updates.hide_from_product_page = null
        } else {
            updates.additional_images = addImages.values.map { it.file.path.replace("\\", "/") }
            updates.additional_image_labels = addImages.values.map { it.label ?: "" }
            updates.hide_from_product_page = addImages.values.filter { it.hidden }.map { it.file.path.replace("\\", "/") }
        }
    }

    private fun setTaggedImage(sku: String, type: ImageType, updates: ImportProductUpdates, file: File?, label: String?) {
        if (type != ImageType.ADDITIONAL) {
            val (imageAttr, labelAttr) = taggedImageAttrs.getValue(type)
            updates.setAttribute(imageAttr, file?.let { imageTools.copyMediaFile(it, sku).path.replace("\\", "/") })
            updates.setAttribute(labelAttr, label)
        }
    }

    companion object {
        val taggedImageAttrs = mapOf(
                ImageType.BASE to listOf(ImportProductUpdates::base_image, ImportProductUpdates::base_image_label),
                ImageType.SMALL to listOf(ImportProductUpdates::small_image, ImportProductUpdates::small_image_label),
                ImageType.THUMBNAIL to listOf(ImportProductUpdates::thumbnail_image, ImportProductUpdates::thumbnail_image_label),
                ImageType.SWATCH to listOf(ImportProductUpdates::swatch_image, ImportProductUpdates::swatch_image_label)
        ).map { entry -> entry.key to entry.value.map { it.toAttribute() } }.toMap()
        val attrToSimpleCompare = mapOf(
                OP::categories to MP::categories,
                OP::attribute_set_code to MP::attribute_set_code,
                OP::product_type to MP::product_type,
                OP::name to MP::name,
                OP::description to MP::description,
                OP::short_description to MP::short_description,
                OP::price to MP::price,
                OP::cost to MP::cost,
                OP::url_key to MP::url_key,
                OP::country_of_manufacture to MP::country_of_manufacture,
                OP::weight to MP::weight,
                OP::meta_title to MP::meta_title,
                OP::meta_description to MP::meta_description,
                OP::meta_keywords to MP::meta_keywords,
                OP::english_name to MP::english_name,
                OP::machine to MP::machine,
                OP::machine_unit to MP::machine_unit,
                OP::machine_vendor to MP::machine_vendor,
                OP::machine_model to MP::machine_model,
                OP::part_vendor to MP::part_vendor,
                OP::catalog_part_number to MP::catalog_part_number,
                OP::ts_dimensions_height to MP::ts_dimensions_height,
                OP::ts_dimensions_length to MP::ts_dimensions_length,
                OP::ts_dimensions_width to MP::ts_dimensions_width
        ).map { it.key.toAttribute() to it.value.toAttribute() }.toMap()
        val imageAttrs = mapOf(
                OP::base_image to (MP::base_image to ImportProductUpdates::base_image),
                OP::small_image to (MP::small_image to ImportProductUpdates::small_image),
                OP::thumbnail_image to (MP::thumbnail_image to ImportProductUpdates::thumbnail_image),
                OP::swatch_image to (MP::swatch_image to ImportProductUpdates::swatch_image)
        ).map { it.key.toAttribute() to (it.value.first.toAttribute() to it.value.second.toAttribute()) }
    }
}
