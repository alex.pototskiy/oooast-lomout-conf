@file:Import("../../entity/MageCategory.lomout.kts")
@file:Import("../../entity/CategoryTree.lomout.kts")

import net.pototskiy.apps.lomout.api.document.toAttribute
import net.pototskiy.apps.lomout.api.plugable.PipelineClassifierPlugin
import net.pototskiy.apps.lomout.api.script.pipeline.ClassifierElement
import org.apache.commons.validator.routines.UrlValidator
import org.jetbrains.kotlin.script.util.Import
import java.io.File
import java.net.URL
import CategoryTree_lomout.CategoryTree as CT
import MageCategory_lomout.MageCategory as MC

class NeedToUpdateCategory : PipelineClassifierPlugin() {
    override fun classify(element: ClassifierElement): ClassifierElement {
        return try {
            val entities = element.entities
            var doUpdate = false
            val mage = entities[MC::class] as MC
            val onec = entities[CT::class] as CT
            for (attr in mapAttr.keys) {
                val mageAttr = mapAttr.getValue(attr)
                if (mage.getAttribute(mageAttr) != onec.getAttribute(attr) &&
                        !isBothIsEmpty(mage.getAttribute(mageAttr), onec.getAttribute(attr))) {
                    doUpdate = true
                }
            }
            if (mage.parentGroupCode != onec.parentSkg) doUpdate = true
            if (mage.image != indexImageName(onec.image) &&
                    !isBothIsEmpty(mage.image, indexImageName(onec.image))) {
                doUpdate = true
            }
            if (doUpdate) element.match() else element.mismatch()
        } catch (e: Exception) {
            logger.error("Cannot compare categories", e)
            element.mismatch()
        }
    }

    private fun indexImageName(image: String?): String? {
        if (image == null) {
            return null
        }
        val urlValidator = UrlValidator(arrayOf("http", "https"))
        val file = if (urlValidator.isValid(image)) {
            URL(image).file
        } else {
            File(image).name
        }
        return "${file[0]}/${file[1]}/$file"
    }

    private fun isBothIsEmpty(value1: Any?, value2: Any?): Boolean {
        return when {
            value1 == null && value2 == null -> true
            value1 == null && value2 is String && value2.isBlank() -> true
            value2 == null && value1 is String && value1.isBlank() -> true
            value1 is String && value1.isBlank() && value2 is String && value2.isBlank() -> true
            else -> false
        }
    }

    companion object {
        val mapAttr = mapOf(
                CT::name.toAttribute() to MC::name.toAttribute(),
                CT::urlKey.toAttribute() to MC::urlKey.toAttribute(),
                CT::description.toAttribute() to MC::description.toAttribute(),
                CT::metaTitle.toAttribute() to MC::metaTitle.toAttribute(),
                CT::metaKeywords.toAttribute() to MC::metaKeywords.toAttribute(),
                CT::metaDescription.toAttribute() to MC::metaDescription.toAttribute()
        )
    }
}
