@file:Import("../../entity/MageProduct.lomout.kts")

import net.pototskiy.apps.lomout.api.AppDataException
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.PipelineClassifier
import net.pototskiy.apps.lomout.api.causesList
import net.pototskiy.apps.lomout.api.script.pipeline.ClassifierElement
import net.pototskiy.apps.lomout.api.suspectedLocation
import org.jetbrains.kotlin.script.util.Import
import org.litote.kmongo.eq
import MageProduct_lomout.MageProduct as MP
import AssembledOnecProduct_lomout.AssembledOnecProduct as OP

class TheSameSku : PipelineClassifier() {
    override operator fun invoke(element: ClassifierElement, context: LomoutContext): ClassifierElement {
        val repository = context.repository
        return try {
            val onecType = OP::class
            val mageType = MP::class
            val onecEntity = element.entities.getOrNull(OP::class) as? OP
            return if (onecEntity != null) {
                val sku = onecEntity.sku
                val mageEntity = repository.get(mageType, MP::sku eq sku, MP::store_view_code eq "admin")
                if (mageEntity != null) {
                    element.match(mageEntity)
                } else {
                    element.mismatch()
                }
            } else {
                val mageEntity = element.entities.getOrNull(MP::class) as? MP
                        ?: throw AppDataException(suspectedLocation(), "There is no proper entity in the pipeline")
                if (mageEntity.store_view_code != "admin") {
                    element.skip()
                } else {
                    val sku = mageEntity.sku
                    val entity = repository.get(onecType, OP::sku eq sku)
                    if (entity != null) {
                        element.skip()
                    } else {
                        element.mismatch()
                    }
                }
            }
        } catch (e: Exception) {
            context.logger.error("Cannot compare product SKUs", e)
            e.causesList { context.logger.error(it) }
            element.mismatch()
        }
    }
}
