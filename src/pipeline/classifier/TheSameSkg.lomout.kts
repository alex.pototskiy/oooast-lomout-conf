@file:Import("../../entity/CategoryTree.lomout.kts")
@file:Import("../../entity/MageCategory.lomout.kts")

import net.pototskiy.apps.lomout.api.plugable.PipelineClassifierPlugin
import net.pototskiy.apps.lomout.api.script.pipeline.ClassifierElement
import org.jetbrains.kotlin.script.util.Import
import org.litote.kmongo.eq
import CategoryTree_lomout.CategoryTree as CT
import MageCategory_lomout.MageCategory as MC

class TheSameSkg : PipelineClassifierPlugin() {
    override fun classify(element: ClassifierElement): ClassifierElement {
        return try {
            var onecEntity = element.entities.getOrNull(CT::class) as? CT
            if (onecEntity != null) {
                val mage = repository.get(MC::class, MC::skg eq onecEntity.skg)
                if (mage != null) {
                    element.match(mage)
                } else {
                    element.mismatch()
                }
            } else if (element.entities.getOrNull(MC::class) != null) {
                val mage = element.entities[MC::class] as MC
                val onec = repository.get(CT::class,CT::skg eq mage.skg)
                if (onec != null) {
                    element.skip()
                } else {
                    element.mismatch()
                }
            } else {
                element.skip()
            }
        } catch (e: Exception) {
            logger.error("Cannot compare category SKGs", e)
            element.mismatch()
        }
    }
}
