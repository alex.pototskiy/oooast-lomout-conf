@file:Import("../../entity/MageCustomerGroup.lomout.kts")
@file:Import("../../entity/OnecCustomerGroup.lomout.kts")

import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.PipelineClassifier
import net.pototskiy.apps.lomout.api.causesList
import net.pototskiy.apps.lomout.api.script.pipeline.ClassifierElement
import org.jetbrains.kotlin.script.util.Import
import MageCustomerGroup_lomout.MageCustomerGroup as MG
import OnecCustomerGroup_lomout.OnecCustomerGroup as OG

class NeedToUpdateGroup : PipelineClassifier() {
    /**
     * Classifier function
     *
     * @param element ClassifierElement The element to classify
     * @return ClassifierElement
     */
    override fun invoke(element: ClassifierElement, context: LomoutContext): ClassifierElement {
        return try {
            val mageGroup = element.entities[MG::class] as MG
            val onecGroup = element.entities[OG::class] as OG
            if (mageGroup.groupName != onecGroup.groupName || mageGroup.taxClass != onecGroup.taxClass) {
                element.match()
            } else {
                element.mismatch()
            }
        } catch (e: Exception) {
            context.logger.error("Cannot compare Magento and Onec customer groups.", e)
            e.causesList { context.logger.error(it) }
            element.skip()
        }
    }

}
