import net.pototskiy.apps.lomout.api.plugable.AttributeWriter
import net.pototskiy.apps.lomout.api.plugable.WriterBuilder
import net.pototskiy.apps.lomout.api.source.workbook.Cell

class BooleanYesNo: AttributeWriter<Boolean?>(), WriterBuilder {
    override fun write(value: Boolean?, cell: Cell) {
        if (value == true) {
            cell.setCellValue("Yes")
        } else {
            cell.setCellValue("No")
        }
    }

    override fun build(): AttributeWriter<out Any?> {
        return BooleanYesNo()
    }
}
