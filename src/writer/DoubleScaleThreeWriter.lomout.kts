import net.pototskiy.apps.lomout.api.entity.writer.DoubleAttributeStringWriter
import net.pototskiy.apps.lomout.api.callable.AttributeWriter
import net.pototskiy.apps.lomout.api.callable.WriterBuilder
import net.pototskiy.apps.lomout.api.callable.createWriter

class DoubleScaleThreeWriter: WriterBuilder {
    override fun build(): AttributeWriter<out Double?> = createWriter<DoubleAttributeStringWriter> {
        scale = 3
    }
}
