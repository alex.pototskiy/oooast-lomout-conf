import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.AttributeWriter
import net.pototskiy.apps.lomout.api.callable.WriterBuilder
import net.pototskiy.apps.lomout.api.source.workbook.Cell

class StoreViewCodeWriter: AttributeWriter<String>(), WriterBuilder {
    override fun build(): AttributeWriter<out Any?> {
        return StoreViewCodeWriter()
    }

    override operator fun invoke(value: String, cell: Cell, context: LomoutContext) {
        if (value != "admin") cell.setCellValue(value)
    }
}
