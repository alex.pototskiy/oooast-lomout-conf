@file:Import("builder/CategoryParentSkg.lomout.kts")
@file:Import("reader/StoreViewCodeReader.lomout.kts")
@file:Import("entity/MageProduct.lomout.kts")
@file:Import("reader/OnecGroupToLongReader.lomout.kts")
@file:Import("reader/OnecSkuReader.lomout.kts")
@file:Import("entity/ImportProduct.lomout.kts")
@file:Import("entity/AssembledOnecProduct.lomout.kts")
@file:Import("entity/OnecProduct.lomout.kts")
@file:Import("pipeline/assembler/AssembleOnecProduct.lomout.kts")
@file:Import("pipeline/assembler/CreateMageProduct.lomout.kts")
@file:Import("pipeline/assembler/DeleteMageProduct.lomout.kts")
@file:Import("pipeline/assembler/UpdateMageProduct.lomout.kts")
@file:Import("pipeline/classifier/NeedToUpdateProduct.lomout.kts")
@file:Import("pipeline/classifier/TheSameSku.lomout.kts")
@file:Import("pipeline/classifier/ValidateOnecProduct.lomout.kts")

import AssembleOnecProduct_lomout.AssembleOnecProduct
import AssembledOnecProduct_lomout.AssembledOnecProduct
import CreateMageProduct_lomout.CreateMageProduct
import DeleteMageProduct_lomout.DeleteMageProduct
import GroupMapping_lomout.GroupMapping
import ImportProduct_lomout.ImportProduct
import MageProduct_lomout.MageProduct
import NeedToUpdateProduct_lomout.NeedToUpdateProduct
import OnecProductAddon_lomout.OnecProductAddon
import OnecProduct_lomout.OnecProduct
import TheSameSku_lomout.TheSameSku
import UpdateMageProduct_lomout.UpdateMageProduct
import ValidateOnecProduct_lomout.ValidateOnecProduct
import net.pototskiy.apps.lomout.api.script.mediator.Pipeline
import net.pototskiy.apps.lomout.api.script.script
import org.jetbrains.kotlin.script.util.Import

script {
    database {
        name("lomout_oooast")
        server {
            host("localhost")
            port(27017)
            user("root")
            password("root")
        }
    }
    val dataFileDir = "data-files"
    loader {
        files {
            file("onec-data") { path("$dataFileDir/input/onec-data.xls"); locale("ru_RU") }
            file("onec-data-for-test") { path("$dataFileDir/input/onec-data-for-test.xls") }
            file("onec-support") { path("$dataFileDir/input/onec-support.xlsm") }
            file("mage-product") { path("$dataFileDir/input/catalog_product.csv"); locale("en_US") }
        }
        load<MageProduct> {
            headersRow(0)
            keepAbsentForDays(10)
            fromSources { source { file("mage-product"); sheet("default"); stopOnEmptyRow() } }
            sourceFields {
                main("product") {
                }
            }
        }
        load<OnecProduct> {
            rowsToSkip(4)
            keepAbsentForDays(10)
            fromSources { source { file("onec-data"); sheet("stock"); stopOnEmptyRow() } }
            sourceFields {
                main("product") {
                    field("sku") {
                        column(0)
                        pattern("""\d+""")
                    } toAttr OnecProduct::sku
                    field("catalog_part_number") { column(1) } toAttr OnecProduct::catalog_part_number
                    field("price_base") { column(2) } toAttr OnecProduct::price_base
                    field("cost") { column(18) } toAttr OnecProduct::cost
                    field("catalog_part_number_ext") { column(3) } toAttr OnecProduct::catalog_part_number_ext
                    field("name") { column(4) } toAttr OnecProduct::name
                    field("english_name") { column(5) } toAttr OnecProduct::english_name
                    field("machine_vendor") { column(6) } toAttr OnecProduct::machine_vendor
                    field("machine_model") { column(7) } toAttr OnecProduct::machine_model
                    field("country_of_manufacture") { column(8) } toAttr OnecProduct::country_of_manufacture
                    field("weight") { column(9) } toAttr OnecProduct::weight
                }
                extra("group") {
                    field("group") {
                        column(0)
                        pattern("""G\d{3}""")
                    } toAttr OnecProduct::group
                }
            }
        }
        load<GroupMapping> {
            rowsToSkip(1)
            keepAbsentForDays(2)
            fromSources { source { file("onec-support"); sheet("group-mapping"); stopOnEmptyRow() } }
            sourceFields {
                main("group-mapping") {
                    field("onec_code") {
                        pattern("""G\d{3,3}""")
                        column(0)
                    } toAttr GroupMapping::onec_code
                    field("mage_code") {
                        pattern("""\d\d\.\d\d\.\d\d\.\d\d\.\d\d""")
                        column(2)
                    } toAttr GroupMapping::mage_code
                }
            }
        }
        load<OnecProductAddon> {
            headersRow(0)
            rowsToSkip(2)
            fromSources { source { file("onec-support"); sheet("product-addon"); stopOnEmptyRow() } }
            sourceFields {
                main("onec-product-addon") {
                    field("sku") { column(0) } toAttr OnecProductAddon::sku
                    field("site_category_codes") { column(1) } toAttr OnecProductAddon::site_category_codes
                }
            }
        }
    }
    mediator {
        produce<AssembledOnecProduct> {
            input { entity(OnecProduct::class) }
            pipeline {
                classifier<ValidateOnecProduct>()
                assembler<AssembleOnecProduct>()
            }
        }
        produce<ImportProduct> {
            input {
                entity(MageProduct::class)
                entity(AssembledOnecProduct::class)
            }
            pipeline {
                classifier<TheSameSku>()
                pipeline(Pipeline.CLASS.MATCHED) {
                    classifier<NeedToUpdateProduct>()
                    assembler<UpdateMageProduct>()
                }
                pipeline(Pipeline.CLASS.UNMATCHED) {
                    classifier { element ->
                        if (element.entities[0].documentMetadata.klass == AssembledOnecProduct::class) {
                            element.match()
                        } else {
                            element.mismatch()
                        }
                    }
                    pipeline(Pipeline.CLASS.MATCHED) {
                        assembler<CreateMageProduct>()
                    }
                    pipeline(Pipeline.CLASS.UNMATCHED) {
                        assembler<DeleteMageProduct>()
                    }
                }
            }
        }
    }
    printer {
        files {
            file("product-add-update") { path("$dataFileDir/output/catalog_product_add_update.csv"); locale("en_US") }
            file("product-delete") { path("$dataFileDir/output/catalog_product_delete.csv"); locale("en_US") }
        }
        print<ImportProduct> {
            input {
                entity(ImportProduct::class)
            }
            output {
                file { file("product-add-update"); sheet("default") }
                printHead = true
                outputFields {
                    main("product") {
                        field("sku")
                        field("store_view_code")
                        field("attribute_set_code")
                        field("product_type")
                        field("product_websites")
                        field("categories")
                        field("name")
                        field("description")
                        field("short_description")
                        field("price")
                        field("cost")
                        field("url_key")
                        field("weight")
                        field("country_of_manufacture")
                        field("meta_title")
                        field("meta_description")
                        field("meta_keywords")
                        // Images
                        field("base_image")
                        field("base_image_label")
                        field("small_image")
                        field("small_image_label")
                        field("thumbnail_image")
                        field("thumbnail_image_label")
                        field("swatch_image")
                        field("swatch_image_label")
                        field("additional_images")
                        field("additional_image_labels")
                        field("hide_from_product_page")
                        // Additional attributes
                        field("ratings_summary")
                        field("ts_dimensions_height")
                        field("ts_dimensions_length")
                        field("ts_dimensions_width")
                        // OO Ast attributes
                        field("english_name")
                        field("machine")
                        field("machine_unit")
                        field("machine_vendor")
                        field("machine_model")
                        field("part_vendor")
                        field("catalog_part_number")
                    }
                }
            }
            pipeline {
                classifier { element ->
                    val action = (element.entities[ImportProduct::class] as ImportProduct)._action
                    if (action != "delete") {
                        element.match()
                    } else {
                        element.mismatch()
                    }
                }
                pipeline(Pipeline.CLASS.MATCHED) {
                    assembler { it.first() as ImportProduct }
                }
            }

        }
        print<ImportProduct> {
            input {
                entity(ImportProduct::class)
            }
            output {
                file { file("product-delete"); sheet("default") }
                printHead = true
                outputFields {
                    main("product") {
                        field("_action")
                        field("sku")
                    }
                }
            }
            pipeline {
                classifier { element ->
                    val action = (element.entities[ImportProduct::class] as ImportProduct)._action
                    if (action == "delete") {
                        element.match()
                    } else {
                        element.mismatch()
                    }
                }
                pipeline(Pipeline.CLASS.MATCHED) {
                    assembler { it.first() as ImportProduct }
                }
            }
        }
    }
}
