import java.io.File
import java.util.*

enum class ImageType {
    BASE, SMALL, SWATCH, THUMBNAIL, ADDITIONAL
}

enum class ImageBase {
    ONEC, MAGE
}

data class ProductImage(
        val sku: String,
        val base: ImageBase,
        val type: ImageType,
        val file: File,
        val label: String? = null,
        val hidden: Boolean = false
) {
    val normalizedFile: File
        get() = when (base) {
            ImageBase.MAGE -> file
            ImageBase.ONEC -> ProductMediaProcessor.fileNameInMagento(file, sku)!!
        }
}

class ProductImageMap(private val data: Map<File, ProductImage>) : Map<File, ProductImage> by data {
    private val tagIndex: Map<ImageType, ProductImage> = data.values
            .filter { it.type != ImageType.ADDITIONAL }
            .associateBy { it.type }

    fun baseImage(): ProductImage? = tagIndex[ImageType.BASE]
    fun smallImage(): ProductImage? = tagIndex[ImageType.SMALL]
    fun thumbnailImage(): ProductImage? = tagIndex[ImageType.THUMBNAIL]
    fun swatchImage(): ProductImage? = tagIndex[ImageType.SWATCH]
}

object ProductMediaProcessor {
    private val fsChar = File.separatorChar
    private val fsString = File.separator!!
    private val inputProductImageDir = File("data-files/images/product")
    private val outputProductImageDir = File("data-files/output/images/product")

    fun getOnecProductImages(sku: String): ProductImageMap {
        val dir = inputProductImageDir.resolve(sku.toList().joinToString(fsString))
        if (!dir.exists()) return ProductImageMap(emptyMap())
        val images = mutableListOf<ProductImage>()
        dir.listFiles()?.filter { it.isFile }?.forEach { file ->
            if (file.extension in arrayOf("png", "jpg")) {
                val properties = Properties()
                File("$file.prop").takeIf { it.exists() }?.reader().use {
                    properties.load(it)
                }
                images.add(ProductImage(
                        sku,
                        ImageBase.ONEC,
                        when (properties.getProperty("type")) {
                            "base" -> ImageType.BASE
                            "small" -> ImageType.SMALL
                            "thumbnail" -> ImageType.THUMBNAIL
                            "swatch" -> ImageType.SWATCH
                            else -> ImageType.ADDITIONAL
                        },
                        file.relativeTo(inputProductImageDir),
                        properties.getProperty("label"),
                        properties.getProperty("hidden")?.toBoolean() ?: false
                ))
            }
        }
        return ProductImageMap(images.associateBy { it.normalizedFile })
    }

    fun copyMediaFile(file: File, sku: String): File {
        val outFile = getOutputMediaName(file, sku)
        val sourceFile = inputProductImageDir.resolve(file)
        val destFile = outputProductImageDir.resolve(outFile)
        destFile.parentFile.mkdirs()
        sourceFile.copyTo(destFile, true)
        return outFile
    }

    fun getOutputMediaName(file: File, sku: String): File {
        return file.parentFile.resolve("${getIndexPrefix(file.name)}.$sku.${file.name}")
    }

    fun fileNameInMagento(file: File?, sku: String): File? {
        if (file == null) return null
        val magentoDir = File(getIndexPrefix(file.name).toList().joinToString(fsString))
        return magentoDir.resolve("${getIndexPrefix(file.name)}.$sku.${file.name}")
    }

    private fun getIndexPrefix(file: String): String {
        var index = 0
        for (element in file) {
            index = (index + element.toInt()) % (indexBase * indexBase)
        }
        return "${(index / indexBase + smallA).toChar()}${(index % indexBase + smallA).toChar()}"
    }


    private const val smallA = 'a'.toInt()
    private const val indexBase = 26
}
