import khttp.structures.authorization.Authorization
import net.pototskiy.apps.lomout.api.LomoutContext
import org.apache.logging.log4j.LogManager
import org.json.JSONArray
import org.json.JSONObject
import java.security.cert.X509Certificate
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

object ServerMachineVendor {
    val labelToValue: Map<String, String>
    val valueToLabel: Map<String, String>

    init {
        val v = RestRequest.get("V1/oooast/catalog/machine-vendor/getAllOptions")
        if (v.statusCode == 200) {
            labelToValue = v.jsonArray.map {
                it as JSONObject
                it.getString("label") to it.getString("value")
            }.toMap()
            valueToLabel = v.jsonArray.map {
                it as JSONObject
                it.getString("value") to it.getString("label")
            }.toMap()
        } else {
            LomoutContext.getContext().logger.error("Cannot load machine vendors list from the server.")
            labelToValue = emptyMap()
            valueToLabel = emptyMap()
        }
    }
}

object ServerPartVendor {
    val labelToValue: Map<String, String>
    val valueToLabel: Map<String, String>

    init {
        val v = RestRequest.get("V1/oooast/catalog/part-vendor/getAllOptions")
        if (v.statusCode == 200) {
            labelToValue = v.jsonArray.map {
                it as JSONObject
                it.getString("label") to it.getString("value")
            }.toMap()
            valueToLabel = v.jsonArray.map {
                it as JSONObject
                it.getString("value") to it.getString("label")
            }.toMap()
        } else {
            LomoutContext.getContext().logger.error("Cannot load part vendors list from the server.")
            labelToValue = emptyMap()
            valueToLabel = emptyMap()
        }
    }
}

object ServerCountries {
    private val logger = LogManager.getLogger("config")
    private val countryValues: Map<String, String>

    init {
        println(System.getProperty("oooast.country.name.lang"))
        val v = RestRequest.get("V1/directory/countries")
        countryValues = if (v.statusCode == 200) {
            try {
                val data = v.jsonArray.mapNotNull {
                    it as JSONObject
                    if (it.isNull("full_name_locale") || it.isNull("full_name_english")) {
                        null
                    } else {
                        it.getString("full_name_locale") to it.getString("full_name_english")
                    }
                }.toMap()
                logger.info("Countries have been loaded from server.")
                data
            } catch (e: Exception) {
                logger.error("Cannot load countries from magento server.")
                logger.trace(e)
                emptyMap<String, String>()
            }
        } else {
            logger.error("Cannot load countries from magento server.")
            emptyMap()
        }
    }

    fun getCountryLocaleValue(country: String): String? = countryValues.keys.find { it.toLowerCase() == correctedCountry(country) }
    fun getCountryEnglishValue(country: String): String? = countryValues[getCountryLocaleValue(country)]

    fun getCountryValue(country: String): String? =
            when (LomoutContext.getContext().getParameter("country.name.lang")?.toLowerCase() ?: "en") {
                "en" -> getCountryEnglishValue(country)
                "ru" -> getCountryLocaleValue(country)
                else -> null
            }

    private fun correctedCountry(country: String): String {
        return countryMap[country.trim()] ?: country.trim().toLowerCase()
    }

    private val countryMap = mapOf(
            "Корея" to "республика корея",
            "UK" to "великобритания"
    )
}

object ServerTaxClasses {
    val taxClasses: List<String>

    init {
        val v = RestRequest.get("V1/taxClasses/search?searchCriteria=*")
        taxClasses = if (v.statusCode == 200) {
            val content = v.jsonObject
            (content["items"] as JSONArray)
                    .filter { (it as JSONObject)["class_type"] == "CUSTOMER" }
                    .map { (it as JSONObject)["class_name"] as String }
        } else {
            LomoutContext.getContext().logger.error("Cannot load tax classes from the server")
            emptyList()
        }
    }

    fun doesTaxClassExist(taxClass: String): Boolean {
        return taxClasses.contains(taxClass)
    }
}

private class MyManager : X509TrustManager {
    override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
    override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
    override fun getAcceptedIssuers(): Array<X509Certificate> {
        return emptyArray()
    }
}

private object RestRequest {
    var accessToken = "8hf5uxx9qshfszydxinxi9q6adiwr9ml"

    init {
        val sc = SSLContext.getInstance("SSL")
        sc.init(null, arrayOf(MyManager()), null)
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
    }

    fun get(url: String): khttp.responses.Response {
        return khttp.get(
                "https://oooast.test/rest/$url",
                headers = mapOf(
                        "Authorization" to "Bearer $accessToken"
                )
        )
    }
}
