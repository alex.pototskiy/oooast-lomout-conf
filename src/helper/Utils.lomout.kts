object ListUtils {
    fun <T : Any> isListEqual(list1: List<T>?, list2: List<T>?): Boolean {
        if (list1 == null && list2 == null) return true
        if (list1?.size != list2?.size) return false
        return list1!!.containsAll(list2!!)
    }
}
