import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.document.DocumentMetadata.Attribute
import net.pototskiy.apps.lomout.api.callable.AttributeReader
import net.pototskiy.apps.lomout.api.callable.ReaderBuilder
import net.pototskiy.apps.lomout.api.source.workbook.Cell

class OnecModelReader : AttributeReader<List<String>?>(), ReaderBuilder {
    override operator fun invoke(attribute: Attribute, input: Cell, context: LomoutContext): List<String>? {
        return Regex("""[^,]+""").findAll(input.asString())
                .mapNotNull { it.groups[0]?.value?.trim() }
                .toList()
    }

    override fun build(): AttributeReader<out Any?> {
        return OnecModelReader()
    }
}
