@file:Import("../helper/ServerData.lomout.kts")

import ServerData_lomout.ServerCountries
import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.AttributeReader
import net.pototskiy.apps.lomout.api.callable.ReaderBuilder
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.entity.reader.StringAttributeReader
import net.pototskiy.apps.lomout.api.source.workbook.Cell
import org.jetbrains.kotlin.script.util.Import

class MageCountryReader : StringAttributeReader(), ReaderBuilder {
    override operator fun invoke(attribute: DocumentMetadata.Attribute, input: Cell, context: LomoutContext): String? {
        return super.invoke(attribute, input, context)?.let { it }
    }

    override fun build(): AttributeReader<out Any?> {
        return MageCountryReader()
    }
}
