import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.callable.AttributeReader
import net.pototskiy.apps.lomout.api.callable.ReaderBuilder
import net.pototskiy.apps.lomout.api.source.workbook.Cell
import net.pototskiy.apps.lomout.api.source.workbook.CellType

class StoreViewCodeReader: AttributeReader<String>(), ReaderBuilder {
    override fun build(): AttributeReader<out Any?> {
        return StoreViewCodeReader()
    }

    override operator fun invoke(attribute: DocumentMetadata.Attribute, input: Cell, context: LomoutContext): String {
        return if (input.cellType == CellType.BLANK || input.asString().isBlank()) {
            "admin"
        } else {
            input.asString()
        }
    }

}
