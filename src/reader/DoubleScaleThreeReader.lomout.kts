import net.pototskiy.apps.lomout.api.entity.reader.DoubleAttributeReader
import net.pototskiy.apps.lomout.api.callable.AttributeReader
import net.pototskiy.apps.lomout.api.callable.ReaderBuilder
import net.pototskiy.apps.lomout.api.callable.createReader

class DoubleScaleThreeReader : ReaderBuilder {
    override fun build(): AttributeReader<out Double?> = createReader<DoubleAttributeReader> {
        scale = 3
    }
}
