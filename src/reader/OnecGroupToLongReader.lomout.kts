import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.DocumentMetadata.Attribute
import net.pototskiy.apps.lomout.api.entity.reader.LongAttributeReader
import net.pototskiy.apps.lomout.api.callable.AttributeReader
import net.pototskiy.apps.lomout.api.callable.ReaderBuilder
import net.pototskiy.apps.lomout.api.source.workbook.Cell

class OnecGroupToLongReader : LongAttributeReader(), ReaderBuilder {
    override operator fun invoke(attribute: Attribute, input: Cell, context: LomoutContext): Long? {
        return input.asString().drop(1).toLong()
    }

    override fun build(): AttributeReader<out Any?> {
        return OnecGroupToLongReader()
    }
}
