import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.AttributeReader
import net.pototskiy.apps.lomout.api.callable.ReaderBuilder
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.entity.reader.StringAttributeReader
import net.pototskiy.apps.lomout.api.source.workbook.Cell
import java.io.File

class OnecCategoryImageReader : StringAttributeReader(), ReaderBuilder {
    override operator fun invoke(attribute: DocumentMetadata.Attribute, input: Cell, context: LomoutContext): String? {
        val image = super.invoke(attribute, input, context)
        return if (image == null || image.isBlank()) {
            null
        } else {
            val imagesLocation = File("data-files/images/category")
            if (imagesLocation.resolve(image).exists()) {
                image
            } else {
                null
            }
        }
    }

    override fun build(): AttributeReader<out Any?> {
        return OnecCategoryImageReader()
    }
}
