import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.document.DocumentMetadata.Attribute
import net.pototskiy.apps.lomout.api.callable.AttributeReader
import net.pototskiy.apps.lomout.api.callable.ReaderBuilder
import net.pototskiy.apps.lomout.api.source.workbook.Cell

class OnecPartNumberReader : AttributeReader<List<String>?>(), ReaderBuilder {
    override operator fun invoke(attribute: Attribute, input: Cell, context: LomoutContext): List<String>? {
        return Regex("""[^,]+""").findAll(input.asString())
                .asSequence()
                .mapNotNull { it.groups[0]?.value?.trim() }
                .map { it.replace("\"", "in ") }
                .map { it.replace("\n", " ") }
                .toList()
    }

    override fun build(): AttributeReader<out Any?> {
        return OnecPartNumberReader()
    }
}
