import net.pototskiy.apps.lomout.api.LomoutContext
import net.pototskiy.apps.lomout.api.callable.AttributeReader
import net.pototskiy.apps.lomout.api.callable.ReaderBuilder
import net.pototskiy.apps.lomout.api.document.DocumentMetadata.Attribute
import net.pototskiy.apps.lomout.api.entity.reader.StringAttributeReader
import net.pototskiy.apps.lomout.api.source.workbook.Cell

class OnecSkuReader : StringAttributeReader(), ReaderBuilder {
    override operator fun invoke(attribute: Attribute, input: Cell, context: LomoutContext): String? {
        return input.asString()
    }

    override fun build(): AttributeReader<out Any?> {
        return OnecSkuReader()
    }
}
