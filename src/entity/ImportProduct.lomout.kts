@file:Import("MageProduct.lomout.kts")

import MageProduct_lomout.MageProduct
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue
import org.jetbrains.kotlin.script.util.Import

open class ImportProduct : MageProduct() {
    var _action: String = initStringValue

    companion object : DocumentMetadata(ImportProduct::class)
}
