@file:Import("MageProduct.lomout.kts")
@file:Import("../builder/ProductImageBuilder.lomout.kts")
@file:Import("../helper/ProductMediaProcessor.lomout.kts")

import MageProduct_lomout.MageProduct
import ProductImageBuilder_lomout.ProductImageBuilder
import ProductMediaProcessor_lomout.ImageBase
import ProductMediaProcessor_lomout.ProductImageMap
import ProductMediaProcessor_lomout.ProductMediaProcessor as mediaTools
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.Index
import net.pototskiy.apps.lomout.api.document.Indexes
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue
import org.bson.codecs.pojo.annotations.BsonIgnore
import org.jetbrains.kotlin.script.util.Import
import java.io.File

class AssembledOnecProduct : MageProduct() {
    @Indexes([Index("url_key_unique_index", Index.SortOrder.ASC, true)])
    override var url_key: String = initStringValue

    @get:BsonIgnore
    override val images: ProductImageMap by lazy {  mediaTools.getOnecProductImages(this.sku) }

    companion object : DocumentMetadata(AssembledOnecProduct::class) {
        private val imageBuilder = ProductImageBuilder().apply {
            imageBase = ImageBase.ONEC
        }
    }
}
