@file:Import("../builder/CategoryParentSkg.lomout.kts")
@file:Import("../reader/OnecCategoryImageReader.lomout.kts")

import CategoryParentSkg_lomout.CategoryParentSkg
import OnecCategoryImageReader_lomout.OnecCategoryImageReader
import net.pototskiy.apps.lomout.api.callable.Reader
import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.Key
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue
import org.bson.codecs.pojo.annotations.BsonIgnore
import org.jetbrains.kotlin.script.util.Import

class CategoryTree : Document() {
    @Key
    var skg: String = initStringValue
    var name: String = initStringValue
    var englishName: String = initStringValue
    var urlKey: String = initStringValue
    @get:BsonIgnore
    val parentSkg: String by lazy { parentSkgBuilder(this) }
    var description: String? = null
    var metaTitle: String = initStringValue
    var metaKeywords: String = initStringValue
    var metaDescription: String? = null
    @Reader(OnecCategoryImageReader::class)
    var image: String? = null

    companion object : DocumentMetadata(CategoryTree::class) {
        val parentSkgBuilder = CategoryParentSkg()
    }
}
