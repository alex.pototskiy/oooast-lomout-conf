import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initDoubleValue
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue

class MageAdvPrice : Document() {
    var sku: String = initStringValue
    var tierPriceWebsite: String = initStringValue
    var tierPriceCustomerGroup: String = initStringValue
    var tierPriceQty: Double = initDoubleValue
    var tierPrice: Double = initDoubleValue
    val tierPriceValueType: String = initStringValue

    companion object : DocumentMetadata(MageAdvPrice::class)
}
