@file:Import("MageCustomerGroup.lomout.kts")

import MageCustomerGroup_lomout.MageCustomerGroup
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue
import org.jetbrains.kotlin.script.util.Import

class ImportCustomerGroup : MageCustomerGroup() {
    var action: String = initStringValue
    
    companion object : DocumentMetadata(ImportCustomerGroup::class)
}


