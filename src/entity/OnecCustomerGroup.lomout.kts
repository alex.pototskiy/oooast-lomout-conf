import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.Key
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue

class OnecCustomerGroup : Document() {
    @Key
    var groupName: String = initStringValue
    var taxClass: String = initStringValue
    var priceGroup: String? = null
    var discount: Double? = null

    companion object : DocumentMetadata(OnecCustomerGroup::class)
}
