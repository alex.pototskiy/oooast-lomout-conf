@file:Import("../reader/StoreViewCodeReader.lomout.kts")
@file:Import("../reader/MageCountryReader.lomout.kts")
@file:Import("../writer/StoreViewCodeWriter.lomout.kts")
@file:Import("../builder/ProductImageBuilder.lomout.kts")
@file:Import("../helper/ProductMediaProcessor.lomout.kts")
@file:Import("../reader/DoubleScaleThreeReader.lomout.kts")
@file:Import("../writer/DoubleScaleThreeWriter.lomout.kts")

import DoubleScaleThreeReader_lomout.DoubleScaleThreeReader
import DoubleScaleThreeWriter_lomout.DoubleScaleThreeWriter
import MageCountryReader_lomout.MageCountryReader
import ProductImageBuilder_lomout.*
import StoreViewCodeReader_lomout.StoreViewCodeReader
import StoreViewCodeWriter_lomout.StoreViewCodeWriter
import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.FieldName
import net.pototskiy.apps.lomout.api.document.Key
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue
import net.pototskiy.apps.lomout.api.entity.reader.DateAttributeReader
import net.pototskiy.apps.lomout.api.entity.reader.DateTimeAttributeReader
import net.pototskiy.apps.lomout.api.entity.reader.LongListAttributeReader
import net.pototskiy.apps.lomout.api.callable.AttributeReader
import net.pototskiy.apps.lomout.api.callable.Reader
import net.pototskiy.apps.lomout.api.callable.ReaderBuilder
import net.pototskiy.apps.lomout.api.callable.Writer
import org.bson.codecs.pojo.annotations.BsonIgnore
import org.jetbrains.kotlin.script.util.Import
import java.time.LocalDate
import java.time.LocalDateTime
import ProductMediaProcessor_lomout.ImageBase
import ProductMediaProcessor_lomout.ProductImageMap
import java.io.File

open class MageProduct : Document() {
    @Key
    var sku: String = initStringValue
    @Key
    @Reader(StoreViewCodeReader::class)
    @Writer(StoreViewCodeWriter::class)
    var store_view_code: String = initStringValue
    var attribute_set_code: String = initStringValue
    var product_type: String = initStringValue
    var categories: List<String>? = null
    var product_websites: List<String>? = null
    var name: String? = null
    var description: String? = null
    var short_description: String? = null
    @Reader(DoubleScaleThreeReader::class)
    @Writer(DoubleScaleThreeWriter::class)
    var weight: Double? = null
    var product_online: Boolean? = null
    var tax_class_name: String? = null
    var visibility: List<String>? = null
    @Reader(DoubleScaleThreeReader::class)
    @Writer(DoubleScaleThreeWriter::class)
    var price: Double? = null
    @Reader(DoubleScaleThreeReader::class)
    @Writer(DoubleScaleThreeWriter::class)
    var cost: Double? = null
    @Reader(DoubleScaleThreeReader::class)
    @Writer(DoubleScaleThreeWriter::class)
    var special_price: Double? = null
    var special_price_from_date: LocalDate? = null
    var special_price_to_date: LocalDate? = null
    open var url_key: String = initStringValue
    var meta_title: String? = null
    var meta_keywords: String? = null
    var meta_description: String? = null
    var base_image: String? = null
    var base_image_label: String? = null
    var small_image: String? = null
    var small_image_label: String? = null
    var thumbnail_image: String? = null
    var thumbnail_image_label: String? = null
    var swatch_image: String? = null
    var swatch_image_label: String? = null
    @Reader(DateTimeReaderBuilder::class)
    var created_at: LocalDateTime? = null
    @Reader(DateTimeReaderBuilder::class)
    var updated_at: LocalDateTime? = null
    @Reader(DateReaderBuilder::class)
    var new_from_date: LocalDate? = null
    @Reader(DateReaderBuilder::class)
    var new_to_date: LocalDate? = null
    @Reader(DoubleScaleThreeReader::class)
    @Writer(DoubleScaleThreeWriter::class)
    var map_price: Double? = null
    @Reader(DoubleScaleThreeReader::class)
    @Writer(DoubleScaleThreeWriter::class)
    var msrp_price: Double? = null
    var map_enabled: Boolean? = null
    var gift_message_available: String? = null
    @Reader(MageCountryReader::class)
    var country_of_manufacture: String? = null
    var qty: Double? = null
    var out_of_stock_qty: Double? = null
    var use_config_min_qty: Boolean? = null
    @FieldName("is_qty_decimal")
    var flag_is_qty_decimal: Boolean? = null
    var allow_backorders: Boolean? = null
    var use_config_backorders: Boolean? = null
    var min_cart_qty: Double? = null
    var use_config_min_sale_qty: Boolean? = null
    var max_cart_qty: Double? = null
    var use_config_max_sale_qty: Boolean? = null
    var is_in_stock: Boolean? = null
    var notify_on_stock_below: Double? = null
    var use_config_notify_stock_qty: Boolean? = null
    var manage_stock: Boolean? = null
    var use_config_manage_stock: Boolean? = null
    var use_config_qty_increments: Boolean? = null
    var qty_increments: Double? = null
    var use_config_enable_qty_inc: Boolean? = null
    var enable_qty_increments: Boolean? = null
    var is_decimal_divided: Boolean? = null
    var website_id: Long? = null
    var related_skus: List<String>? = null
    @Reader(LongListReaderBuilder::class)
    var related_position: List<Long>? = null
    var crosssell_skus: List<String>? = null
    @Reader(LongListReaderBuilder::class)
    var crosssell_position: List<Long>? = null
    var upsell_skus: List<String>? = null
    @Reader(LongListReaderBuilder::class)
    var upsell_position: List<Long>? = null
    var additional_images: List<String>? = null
    var additional_image_labels: List<String>? = null
    var hide_from_product_page: List<String>? = null
    var custom_options: List<String>? = null

    var display_product_options_in: String? = null
    var custom_design: String? = null
    @Reader(DateReaderBuilder::class)
    var custom_design_from: LocalDate? = null
    @Reader(DateReaderBuilder::class)
    var custom_design_to: LocalDate? = null
    var custom_layout_update: String? = null
    var page_layout: String? = null
    var product_options_container: String? = null
    var msrp_display_actual_price_type: String? = null
    var bundle_price_type: String? = null
    var bundle_sku_type: String? = null
    var bundle_price_view: String? = null
    var bundle_weight_type: String? = null
    var bundle_values: String? = null
    var bundle_shipment_type: String? = null
    var associated_skus: String? = null
    var configurable_variations: String? = null
    var configurable_variation_labels: String? = null
    // former additional attributes
    var ratings_summary: Double? = null
    var english_name: String? = null
    var machine: String? = null
    var machine_unit: String? = null
    var machine_vendor: List<String>? = null
    var machine_model: List<String>? = null
    var part_vendor: List<String>? = null
    var catalog_part_number: List<String>? = null
    @Reader(DoubleScaleThreeReader::class)
    @Writer(DoubleScaleThreeWriter::class)
    var ts_dimensions_height: Double? = null
    @Reader(DoubleScaleThreeReader::class)
    @Writer(DoubleScaleThreeWriter::class)
    var ts_dimensions_length: Double? = null
    @Reader(DoubleScaleThreeReader::class)
    @Writer(DoubleScaleThreeWriter::class)
    var ts_dimensions_width: Double? = null

    @get:BsonIgnore
    open val images: ProductImageMap by lazy {  imageBuilder(this) }

    companion object : DocumentMetadata(MageProduct::class) {
        private val imageBuilder = ProductImageBuilder().apply {
            imageBase = ImageBase.MAGE
        }
    }

    class LongListReaderBuilder : LongListAttributeReader(), ReaderBuilder {
        override fun build(): AttributeReader<out Any?> {
            return LongListReaderBuilder().apply {
                groupingUsed = false
            }
        }
    }

    class DateTimeReaderBuilder : DateTimeAttributeReader(), ReaderBuilder {
        override fun build(): AttributeReader<out Any?> {
            return DateTimeReaderBuilder().apply {
                pattern = "d.M.uuuu, H:m"
            }
        }
    }

    class DateReaderBuilder : DateAttributeReader(), ReaderBuilder {
        override fun build(): AttributeReader<out Any?> {
            return DateReaderBuilder().apply {
                pattern = "d.M.uuuu"
            }
        }
    }
}
