@file:Import("MageCategory.lomout.kts")

import MageCategory_lomout.MageCategory
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue

class ImportCategory : MageCategory() {
    var action: String = initStringValue

    companion object : DocumentMetadata(ImportCategory::class)
}
