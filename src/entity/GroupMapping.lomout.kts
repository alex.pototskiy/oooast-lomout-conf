@file:Import("../reader/OnecGroupToLongReader.lomout.kts")

import OnecGroupToLongReader_lomout.OnecGroupToLongReader
import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.Key
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initLongValue
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue
import net.pototskiy.apps.lomout.api.callable.Reader
import org.jetbrains.kotlin.script.util.Import

class GroupMapping : Document() {
    @Key
    @Reader(OnecGroupToLongReader::class)
    var onec_code: Long = initLongValue
    @Key
    var mage_code: String = initStringValue

    companion object : DocumentMetadata(GroupMapping::class)
}
