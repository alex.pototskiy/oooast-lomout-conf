@file:Import("ImportProduct.lomout.kts")

import ImportProduct_lomout.ImportProduct
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import org.jetbrains.kotlin.script.util.Import

class ImportProductUpdates : ImportProduct() {
    companion object : DocumentMetadata(ImportProductUpdates::class)
}
