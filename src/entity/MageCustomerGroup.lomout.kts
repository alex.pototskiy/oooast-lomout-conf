import net.pototskiy.apps.lomout.api.document.*
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initIntValue
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue

open class MageCustomerGroup : Document() {
    @Key
    var groupId: Int = initIntValue
    @Key
    var groupName: String = initStringValue
    var taxClass: String = initStringValue

    companion object : DocumentMetadata(MageCustomerGroup::class)
}
