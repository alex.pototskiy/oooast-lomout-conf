@file:Import("../reader/OnecGroupToLongReader.lomout.kts")
@file:Import("../reader/OnecPartNumberReader.lomout.kts")
@file:Import("../reader/OnecSkuReader.lomout.kts")
@file:Import("../builder/OnecProductCategories.lomout.kts")
@file:Import("../builder/ExtractOnecVendor.lomout.kts")
@file:Import("../reader/OnecModelReader.lomout.kts")
@file:Import("../reader/DoubleScaleThreeReader.lomout.kts")

import OnecModelReader_lomout.OnecModelReader
import ExtractOnecVendor_lomout.ExtractOnecVendor
import OnecGroupToLongReader_lomout.OnecGroupToLongReader
import OnecPartNumberReader_lomout.OnecPartNumberReader
import OnecProductCategories_lomout.OnecProductCategories
import OnecSkuReader_lomout.OnecSkuReader
import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.Key
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initLongValue
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue
import net.pototskiy.apps.lomout.api.callable.Reader
import org.bson.codecs.pojo.annotations.BsonIgnore
import org.jetbrains.kotlin.script.util.Import
import DoubleScaleThreeReader_lomout.DoubleScaleThreeReader

class OnecProduct : Document() {
    @Key
    @Reader(OnecSkuReader::class)
    var sku: String = initStringValue
    @Reader(OnecGroupToLongReader::class)
    var group: Long = initLongValue
    @Reader(DoubleScaleThreeReader::class)
    var price_base: Double? = null
    @Reader(DoubleScaleThreeReader::class)
    var cost: Double? = null
    @Reader(OnecPartNumberReader::class)
    var catalog_part_number: List<String>? = null
    @Reader(OnecPartNumberReader::class)
    var catalog_part_number_ext: List<String>? = null
    var name: String? = null
    var english_name: String? = null
    var machine_vendor: String? = null
    @Reader(OnecModelReader::class)
    var machine_model: List<String>? = null
    var country_of_manufacture: String? = null
    @Reader(DoubleScaleThreeReader::class)
    var weight: Double? = null

    @get:BsonIgnore
    val categories: List<String> by lazy { categoriesBuilder(this) }
    @get:BsonIgnore
    val machine_vendor_list: List<String> by lazy { ExtractOnecVendor.extractMachineVendor(this) }
    @get:BsonIgnore
    val part_vendor_list: List<String> by lazy { ExtractOnecVendor.extractPartVendor(this) }

    companion object : DocumentMetadata(OnecProduct::class) {
        val categoriesBuilder = OnecProductCategories().apply {
            pathCategorySeparator = "/"
            pathPrefix = "Default Category/OOO Ast Catalog/"
        }
    }
}
