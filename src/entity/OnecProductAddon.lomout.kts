@file:Import("../reader/OnecSkuReader.lomout.kts")
@file:Import("../reader/OnecPartNumberReader.lomout.kts")
@file:Import("../reader/OnecModelReader.lomout.kts")

import OnecModelReader_lomout.OnecModelReader
import OnecPartNumberReader_lomout.OnecPartNumberReader
import OnecSkuReader_lomout.OnecSkuReader
import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.Key
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue
import net.pototskiy.apps.lomout.api.callable.Reader
import org.jetbrains.kotlin.script.util.Import

class OnecProductAddon : Document() {
    @Key
    @Reader(OnecSkuReader::class)
    var sku: String = initStringValue
    var site_category_codes: List<String>? = null
    var attribute_set_code: String? = null
    var product_type: String? = null
    var name: String? = null
    var english_name: String? = null
    var description: String? = null
    var short_description: String? = null
    var machine: String? = null
    var machine_unit: String? = null
    var machine_vendor: List<String>? = null
    @Reader(OnecModelReader::class)
    var machine_model: List<String>? = null
    var part_vendor: List<String>? = null
    @Reader(OnecPartNumberReader::class)
    var catalog_part_number: List<String>? = null
    // dimensions
    var height: Double? = null
    var length: Double? = null
    var width: Double? = null
    // meta
    var meta_title: String? = null
    var meta_description: String? = null
    var meta_keywords: String? = null

    companion object : DocumentMetadata(OnecProductAddon::class)
}
