@file:Import("../writer/BooleanYesNo.lomout.kts")

import BooleanYesNo_lomout.BooleanYesNo
import net.pototskiy.apps.lomout.api.callable.Writer
import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.document.DocumentMetadata
import net.pototskiy.apps.lomout.api.document.Key
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initBooleanValue
import net.pototskiy.apps.lomout.api.document.SupportAttributeType.initStringValue
import net.pototskiy.apps.lomout.api.plugable.Writer
import org.jetbrains.kotlin.script.util.Import
import java.time.LocalDate

open class MageCategory : Document() {
    @Key
    var skg: String = initStringValue
    var name: String = initStringValue
    var parentGroupCode: String = initStringValue
    var store: String? = null
    var website: String? = null
    @Writer(BooleanYesNo::class)
    var flagIsActive: Boolean = initBooleanValue
    var description: String? = null
    var metaTitle: String? = null
    var metaDescription: String? = null
    var metaKeywords: String? = null
    var landingPage: String? = null
    var flagIsAnchor: Boolean = initBooleanValue
    var customDesign: String? = null
    var customDesignFrom: LocalDate? = null
    var customDesignTo: LocalDate? = null
    var pageLayout: String? = null
    var customLayoutUpdate: String? = null
    var availableSorBy: List<String>? = null
    var defaultSorBy: List<String>? = null
    @Writer(BooleanYesNo::class)
    var includeInMenu: Boolean = initBooleanValue
    @Writer(BooleanYesNo::class)
    var customUseParentSettings: Boolean? = null
    @Writer(BooleanYesNo::class)
    var customApplyToProducts: Boolean? = null
    var urlKey: String = initStringValue
    @Writer(BooleanYesNo::class)
    var useNameInProductSearch: Boolean = initBooleanValue
    var flagIsVirtualCategory: Boolean? = null
    var virtualCategoryRoot: String? = null
    var virtualRule: String? = null
    var useStorePositions: Boolean = initBooleanValue

    var displayMode: String = initStringValue
    var image: String? = null
    var filter_price_range: String? = null


    companion object : DocumentMetadata(MageCategory::class)
}
