import net.pototskiy.apps.lomout.api.document.Document
import net.pototskiy.apps.lomout.api.document.DocumentMetadata

class OnecProductSupport : Document() {
    companion object : DocumentMetadata(OnecProductSupport::class)
}
