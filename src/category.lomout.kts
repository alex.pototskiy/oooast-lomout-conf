@file:Import("entity/CategoryTree.lomout.kts")
@file:Import("entity/MageCategory.lomout.kts")
@file:Import("pipeline/assembler/CreateMageCategory.lomout.kts")
@file:Import("pipeline/assembler/UpdateMageCategory.lomout.kts")
@file:Import("pipeline/assembler/DeleteMageCategory.lomout.kts")
@file:Import("pipeline/classifier/TheSameSkg.lomout.kts")
@file:Import("pipeline/classifier/NeedToUpdateCategory.lomout.kts")

import CategoryTree_lomout.CategoryTree
import CreateMageCategory_lomout.CreateMageCategory
import DeleteMageCategory_lomout.DeleteMageCategory
import ImportCategory_lomout.ImportCategory
import MageCategory_lomout.MageCategory
import NeedToUpdateCategory_lomout.NeedToUpdateCategory
import TheSameSkg_lomout.TheSameSkg
import UpdateMageCategory_lomout.UpdateMageCategory
import net.pototskiy.apps.lomout.api.script.mediator.Pipeline
import net.pototskiy.apps.lomout.api.script.script
import org.jetbrains.kotlin.script.util.Import

script {
    database {
        name("lomout_oooast")
        server {
            host("localhost")
            port(27017)
            user("root")
            password("root")
        }
    }
    val dataFileDir = "data-files"
    loader {
        files {
            file("onec-data") { path("$dataFileDir/input/test-products.xls") }
            file("onec-support") { path("$dataFileDir/input/onec-support.xlsm") }
            file("mage-category") { path("$dataFileDir/input/catalog_category.csv") }
        }
        load<CategoryTree> {
            headersRow(0)
            keepAbsentForDays(10)
            fromSources { source { file("onec-support"); sheet("category-tree"); stopOnEmptyRow() } }
            sourceFields {
                main("category") {
                    field("skg") {
                        pattern("""\d\d\.\d\d\.\d\d\.\d\d\.\d\d""")
                    } toAttr CategoryTree::skg
                    field("name") toAttr CategoryTree::name
                    field("english-name") toAttr CategoryTree::englishName
                    field("url-key") toAttr CategoryTree::urlKey
                    field("image") toAttr CategoryTree::image
                    field("description") toAttr CategoryTree::description
                    field("meta-title") toAttr CategoryTree::metaTitle
                    field("meta-keywords") toAttr CategoryTree::metaKeywords
                    field("meta-description") toAttr CategoryTree::metaDescription
                }
            }
        }
        load<MageCategory> {
            headersRow(0)
            keepAbsentForDays(10)
            fromSources { source { file("mage-category"); sheet(Regex(".*")); stopOnEmptyRow() } }
            sourceFields {
                main("category") {
                    field("_store") toAttr MageCategory::store
                    field("_website") toAttr MageCategory::website
                    field("_parent_group_code") toAttr MageCategory::parentGroupCode
                    field("skg") toAttr MageCategory::skg
                    field("name") toAttr MageCategory::name
                    field("is_active") toAttr MageCategory::flagIsActive
                    field("description") toAttr MageCategory::description
                    field("image") toAttr MageCategory::image
                    field("meta_title") toAttr MageCategory::metaTitle
                    field("meta_keywords") toAttr MageCategory::metaKeywords
                    field("meta_description") toAttr MageCategory::metaDescription
                    field("display_mode") toAttr MageCategory::displayMode
                    field("landing_page") toAttr MageCategory::landingPage
                    field("is_anchor") toAttr MageCategory::flagIsAnchor
                    field("custom_design") toAttr MageCategory::customDesign
                    field("custom_design_from") toAttr MageCategory::customDesignFrom
                    field("custom_design_to") toAttr MageCategory::customDesignTo
                    field("page_layout") toAttr  MageCategory::pageLayout
                    field("custom_layout_update") toAttr MageCategory::customLayoutUpdate
                    field("available_sort_by") toAttr MageCategory::availableSorBy
                    field("default_sort_by") toAttr MageCategory::defaultSorBy
                    field("include_in_menu") toAttr MageCategory::includeInMenu
                    field("custom_use_parent_settings") toAttr MageCategory::customUseParentSettings
                    field("custom_apply_to_products") toAttr MageCategory::customApplyToProducts
                    field("url_key") toAttr MageCategory::urlKey
                    field("use_name_in_product_search") toAttr MageCategory::useNameInProductSearch
                    field("is_virtual_category") toAttr MageCategory::flagIsVirtualCategory
                    field("virtual_category_root") toAttr MageCategory::virtualCategoryRoot
                    field("virtual_rule") toAttr MageCategory::virtualRule
                    field("use_store_positions") toAttr MageCategory::useStorePositions
                }
            }
        }
    }
    mediator {
        produce<ImportCategory> {
            input {
                entity(CategoryTree::class)
                entity(MageCategory::class)
            }
            pipeline {
                classifier<TheSameSkg>()
                pipeline(Pipeline.CLASS.MATCHED) {
                    classifier<NeedToUpdateCategory>()
                    pipeline(Pipeline.CLASS.MATCHED) {
                        assembler<UpdateMageCategory>()
                    }
                }
                pipeline(Pipeline.CLASS.UNMATCHED) {
                    classifier { element ->
                        if (element.entities[0].documentMetadata.klass == CategoryTree::class) {
                            element.match()
                        } else {
                            element.mismatch()
                        }
                    }
                    pipeline(Pipeline.CLASS.MATCHED) {
                        assembler<CreateMageCategory>()
                    }
                    pipeline(Pipeline.CLASS.UNMATCHED) {
                        assembler<DeleteMageCategory>()
                    }
                }
            }
        }
    }
    printer {
        files {
            file("category-add-update") { path("$dataFileDir/output/catalog_category_add_update.csv") }
            file("category-delete") { path("$dataFileDir/output/catalog_category_delete.csv") }
        }
        print<ImportCategory> {
            input {
                entity(ImportCategory::class)
            }
            output {
                file { file("category-add-update"); sheet("default") }
                printHead = true
                outputFields {
                    main("category") {
                        field("_action") fromAttr ImportCategory::action
                        field("_store") fromAttr ImportCategory::store
                        field("_website") fromAttr ImportCategory::website
                        field("_parent_group_code") fromAttr ImportCategory::parentGroupCode
                        field("skg") fromAttr ImportCategory::skg
                        field("name") fromAttr ImportCategory::name
                        field("is_active") fromAttr ImportCategory::flagIsActive
                        field("display_mode") fromAttr ImportCategory::displayMode
                        field("is_anchor") fromAttr ImportCategory::flagIsAnchor
                        field("include_in_menu") fromAttr ImportCategory::includeInMenu
                        field("use_name_in_product_search") fromAttr ImportCategory::useNameInProductSearch
                        field("url_key") fromAttr ImportCategory::urlKey
                        field("image") fromAttr ImportCategory::image
                        field("description") fromAttr ImportCategory::description
                        field("meta_title") fromAttr ImportCategory::metaTitle
                        field("meta_keywords") fromAttr ImportCategory::metaKeywords
                        field("meta_description") fromAttr ImportCategory::metaDescription
                    }
                }
            }
            pipeline {
                classifier {
                    val entity = it.entities[ImportCategory::class] as ImportCategory
                    if ("delete" != entity.action) {
                        it.match()
                    } else {
                        it.mismatch()
                    }
                }
                pipeline(Pipeline.CLASS.MATCHED) {
                    assembler { it.first() as ImportCategory }
                }
            }
        }
        print<ImportCategory> {
            input {
                entity(ImportCategory::class)
            }
            output {
                file { file("category-delete"); sheet("default") }
                printHead = true
                outputFields {
                    main("category") {
                        field("_action") fromAttr ImportCategory::action
                        field("_store") fromAttr ImportCategory::store
                        field("_website") fromAttr ImportCategory::website
                        field("_parent_group_code") fromAttr ImportCategory::parentGroupCode
                        field("skg") fromAttr ImportCategory::skg
                        field("name") fromAttr ImportCategory::name
                        field("is_active") fromAttr ImportCategory::flagIsActive
                        field("display_mode") fromAttr ImportCategory::displayMode
                        field("is_anchor") fromAttr ImportCategory::flagIsAnchor
                        field("include_in_menu") fromAttr ImportCategory::includeInMenu
                        field("use_name_in_product_search") fromAttr ImportCategory::useNameInProductSearch
                        field("url_key") fromAttr ImportCategory::urlKey
                        field("image") fromAttr ImportCategory::image
                        field("description") fromAttr ImportCategory::description
                        field("meta_title") fromAttr ImportCategory::metaTitle
                        field("meta_keywords") fromAttr ImportCategory::metaKeywords
                        field("meta_description") fromAttr ImportCategory::metaDescription
                    }
                }
            }
            pipeline {
                classifier {
                    val entity = it.entities[ImportCategory::class] as ImportCategory
                    if (entity.action == "delete") {
                        it.match()
                    } else {
                        it.mismatch()
                    }
                }
                pipeline(Pipeline.CLASS.MATCHED) {
                    assembler { it.first() as ImportCategory }
                }
            }
        }
    }
}
