@file:Import("entity/MageCustomerGroup.lomout.kts")
@file:Import("entity/OnecCustomerGroup.lomout.kts")
@file:Import("entity/ImportCustomerGroup.lomout.kts")
@file:Import("pipeline/classifier/TheSameGroupName.lomout.kts")
@file:Import("pipeline/classifier/NeedToUpdateGroup.lomout.kts")
@file:Import("pipeline/assembler/CreateMageCustomerGroup.lomout.kts")
@file:Import("pipeline/assembler/DeleteMageCustomerGroup.lomout.kts")

import CreateMageCustomerGroup_lomout.CreateMageCustomerGroup
import DeleteMageCustomerGroup_lomout.DeleteMageCustomerGroup
import ImportCustomerGroup_lomout.ImportCustomerGroup
import MageCustomerGroup_lomout.MageCustomerGroup
import NeedToUpdateGroup_lomout.NeedToUpdateGroup
import OnecCustomerGroup_lomout.OnecCustomerGroup
import TheSameGroupName_lomout.TheSameGroupName
import net.pototskiy.apps.lomout.api.script.mediator.Pipeline
import net.pototskiy.apps.lomout.api.script.script
import org.jetbrains.kotlin.script.util.Import

script {
    database {
        name("lomout_oooast")
        server {
            host("localhost")
            port(27017)
            user("root")
            password("root")
        }
    }
    val dataFileDir = "data-files"
    loader {
        files {
            file("onec-support") { path("$dataFileDir/input/onec-support.xlsm") }
            file("mage-groups") { path("$dataFileDir/input/customer_group.csv"); locale("en_US") }
        }
        load<OnecCustomerGroup> {
            headersRow(0)
            keepAbsentForDays(10)
            fromSources { source { file("onec-support"); sheet("customer-groups"); stopOnEmptyRow() } }
            sourceFields {
                main("group") {
                    field("name") toAttr OnecCustomerGroup::groupName
                    field("tax_class") toAttr OnecCustomerGroup::taxClass
                }
            }
        }
        load<MageCustomerGroup> {
            headersRow(0)
            keepAbsentForDays(10)
            fromSources { source { file("mage-groups"); sheet("default"); stopOnEmptyRow() } }
            sourceFields {
                main("group") {
                    field("customer_group_id") toAttr MageCustomerGroup::groupId
                    field("customer_group_code") toAttr MageCustomerGroup::groupName
                    field("tax_class") toAttr MageCustomerGroup::taxClass
                }
            }
        }
    }
    mediator {
        produce<ImportCustomerGroup> {
            input {
                entity(MageCustomerGroup::class)
                entity(OnecCustomerGroup::class)
            }
            pipeline {
                classifier<TheSameGroupName>()
                pipeline(Pipeline.CLASS.MATCHED) {
                    classifier<NeedToUpdateGroup>()
                    assembler<CreateMageCustomerGroup>()
                }
                pipeline(Pipeline.CLASS.UNMATCHED) {
                    classifier { element ->
                        if (element.entities[0].documentMetadata.klass == OnecCustomerGroup::class) {
                            element.match()
                        } else {
                            element.mismatch()
                        }
                    }
                    pipeline(Pipeline.CLASS.MATCHED) {
                        assembler<CreateMageCustomerGroup>()
                    }
                    pipeline(Pipeline.CLASS.UNMATCHED) {
                        assembler<DeleteMageCustomerGroup>()
                    }
                }
            }
        }
    }
    printer {
        files {
            file("group-add-update") { path("$dataFileDir/output/customer_group_add_update.csv"); locale("en_US") }
            file("group-delete") { path("$dataFileDir/output/customer_group_delete.csv"); locale("en_US") }
        }
        print<ImportCustomerGroup> {
            input { entity(ImportCustomerGroup::class) }
            output {
                file { file("group-add-update"); sheet("default") }
                printHead = true
                outputFields {
                    main("group") {
                        field("customer_group_id") fromAttr ImportCustomerGroup::groupId
                        field("customer_group_code") fromAttr ImportCustomerGroup::groupName
                        field("tax_class") fromAttr ImportCustomerGroup::taxClass
                    }
                }
            }
            pipeline {
                classifier { element ->
                    val action = (element.entities[ImportCustomerGroup::class] as ImportCustomerGroup).action
                    if (action != "delete") {
                        element.match()
                    } else {
                        element.mismatch()
                    }
                }
                pipeline(Pipeline.CLASS.MATCHED) {
                    assembler { it.first() as ImportCustomerGroup }
                }
            }
        }
        print<ImportCustomerGroup> {
            input { entity(ImportCustomerGroup::class) }
            output {
                file { file("group-delete"); sheet("default") }
                printHead = true
                outputFields {
                    main("group") {
                        field("customer_group_id") fromAttr ImportCustomerGroup::groupId
                        field("customer_group_code") fromAttr ImportCustomerGroup::groupName
                        field("tax_class") fromAttr ImportCustomerGroup::taxClass
                    }
                }
            }
            pipeline {
                classifier { element ->
                    val action = (element.entities[ImportCustomerGroup::class] as ImportCustomerGroup).action
                    if (action == "delete") {
                        element.match()
                    } else {
                        element.mismatch()
                    }
                }
                pipeline(Pipeline.CLASS.MATCHED) {
                    assembler { it.first() as ImportCustomerGroup }
                }
            }
        }
    }
}
